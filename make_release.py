#! /usr/bin/env python
from __future__ import print_function
import os

if not os.path.isdir("bin"):
	os.mkdir("bin")
	
os.chdir("bin")
os.system('cmake -DCMAKE_BUILD_TYPE=Release -DBUILD_EXECUTABLE=ON -DBUILD_TESTS=ON ../src')
os.system("make -j4")
os.chdir("..")
