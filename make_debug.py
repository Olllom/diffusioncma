#! /usr/bin/env python
from __future__ import print_function
import os

if not os.path.isdir("debug"):
	os.mkdir("debug")
	
os.chdir("debug")
os.system('cmake -G "Eclipse CDT4 - Unix Makefiles" -DCMAKE_INSTALL_PREFIX=../install -DCMAKE_BUILD_TYPE=Debug -DCMAKE_ECLIPSE_GENERATE_SOURCE_PROJECT=TRUE -DCMAKE_ECLIPSE_MAKE_ARGUMENTS=-j8 -DBUILD_EXECUTABLE=ON -DBUILD_TESTS=ON ../src')
os.system("make -j4")
os.chdir("..")
