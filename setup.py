import os
import re
import sys
import shutil
import platform
import subprocess

import versioneer

from setuptools import setup, Command
from setuptools.command.install import install
from distutils.version import LooseVersion


def prompt_yn(message):
    answer = input(message + " (y/n)" + os.linesep)
    while answer not in ["y", "n"]:
        answer = input("Please type y or n" + os.linesep)
    return answer == "y"


class InstallDCMACommand(install):
    description = "Custom install command that checks for the wrappers."

    def run(self):
        print("Installing the dcma package. " +
              "---- Not updating binaries. ---- ".upper() +
              "To update the binaries run 'python setup.py compile'. ")
        try:
            import pydiffusioncma
            install.run(self)
        except ImportError:
            print("Binaries not found.".upper() + " For an easy way to install "
                  "diffusioncma through conda, check out "
                  "https://gitlab.com/Olllom/diffusioncma. "
                  "If you are really sure that you want to compile the code "
                  "run python setup.py compile. But be warned: "
                  "Getting the third-party dependencies working without conda "
                  "might be painful.")
            if prompt_yn("Would you like to continue without the core part?"):
                install.run(self)
            else:
                print("Aborting")
                sys.exit(1)


class CompileCppCommand(Command):
    description = ("Recompiles diffusioncma binaries "
                   "and installs the python wrappers.")

    user_options = [
        ("debug", None, "whether to compile in debug mode"),
        ("openmp", None, "whether to use openmp"),
    ]

    def initialize_options(self):
        self.debug = False
        self.openmp = False

    def finalize_options(self):
        if self.debug != False:
            self.debug = True
        if self.openmp != False:
            self.openmp = True

    def run(self):
        self.check_cmake_version()
        if not os.path.isdir("build"):
            os.mkdir("build")
        self.check_cmake_version()
        self.cmake_build()

    def cmake_build(self):
        env = os.environ.copy()
        cmake_args = [
            '-DPYTHON_EXECUTABLE=' + sys.executable,
            '-DCMAKE_INSTALL_PREFIX=' + os.path.dirname(os.path.dirname(sys.executable)),
            '-DCMAKE_BUILD_TYPE=' + ('Debug' if self.debug else 'Release'),
            '-DUSE_OPENMP=' + ('ON' if self.openmp else 'OFF'),
            '-B.'
        ]

        subprocess.check_call(
            ['cmake', "../src"] + cmake_args,
            cwd="build", env=env)
        print("Cmake call: ", " ".join(['cmake', "../src"] + cmake_args))
        subprocess.check_call(['make', '-j4', 'install'], cwd="build")
        subprocess.check_call(['make', 'install_python_wrappers'], cwd="build")

    def check_cmake_version(self):
        try:
            cmake_version_out = subprocess.check_output(['cmake', '--version'])
        except OSError:
            raise RuntimeError("CMake must be installed to build the "
                               "following extensions: " +
                               ", ".join(e.name for e in self.extensions)
                              )
        # get version
        if platform.system() == "Windows":
            cmake_version = LooseVersion(
                re.search(r'version\s*([\d.]+)',cmake_version_out.decode()
                          ).group(1))
            if cmake_version < '3.1.0':
                raise RuntimeError("CMake >= 3.1.0 is required on Windows")


class CleanCppCommand(Command):
    description = ("Cleans up the build directory so that the next time "
                   "diffusioncma is built, it is built from scratch.")
    user_options = []

    def run(self):
        if os.path.isdir("build"):
            shutil.rmtree("build")
            print("build directory removed")
        else:
            print("already clean")

    def initialize_options(self):
        pass
    def finalize_options(self):
        pass


class TestCommand(Command):
    description = ("Runs test suite (after installation).")
    user_options = []

    def run(self):
        os.system("pytest --pyargs dcma.testsuite")

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass


custom_commands = {
    'install': InstallDCMACommand,
    'compile': CompileCppCommand,
    'clean': CleanCppCommand,
    'test': TestCommand
}


def merge_dicts(a,b):
    a.update(b)
    return a


setup(
    name='dcma',
    version=versioneer.get_version(),
    author='Andreas Kraemer',
    author_email='kraemer.research@gmail.com',
    description='A program to calculate diffusion and free energy profiles in membranes.',
    long_description='',
    packages=['dcma', 'dcma.testsuite'],
    package_dir = {'dcma': 'src/python/dcma', 'dcma.testsuite': 'src/python/tests'},
    entry_points={
        'console_scripts': [
            'dcma=dcma.cli:main'
        ]
    },
    include_package_data=True,
    package_data={"dcma.testsuite": ["oxygen.*.txt", "tmat*.txt", "log.out", "log.out.coco"]},
    #install_requires=requirements,
    #test_suite='dcma_tests',
    #tests_require=test_requirements,
    #setup_requires=setup_requirements,
    classifiers=[
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    cmdclass=merge_dicts(versioneer.get_cmdclass(), custom_commands), #dict(versioneer.get_cmdclass().update(custom_commands)),
    zip_safe=False,
)
