
#include "diffusioncma/GlobalDefinitions.h"
#include "diffusioncma/Config.h"
#include "diffusioncma/ConfigFromArgs.h"
#include "diffusioncma/Util.h"
#include "diffusioncma/Optimization.h"
#include "cmaes.h"

using namespace libcmaes;
using namespace diffusioncma;


int main(int argc, char *argv[])
{
	try {
		MainConfig cfg = config_from_args(argc, argv);
		Solution sol = optimize(cfg);
		CMASolutions cmasols = sol.getCMASolutions();
		std::cout << "best solution: " << cmasols << std::endl;
		std::cout << "optimization took " << cmasols.elapsed_time() / 1000.0 << " seconds\n";
	} catch (DiffusionCMAError& e){
		cerr << "Error in Diffusion CMA." << endl;
		cerr << e.what() << endl;
		cerr << "Exiting." << endl;
		exit(1);
	}
}

