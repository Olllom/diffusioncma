/*
 * Optimization_test.cpp
 *
 *  Created on: 24.05.2018
 *      Author: akraemer
 */

#include "diffusioncma/Optimization.h"


#include "catch.hpp"
#include "diffusioncma/Util.h"
#include "diffusioncma/LogLikelihood.h"
#include "diffusioncma/Profiles.h"
#include "SharedFiles.h"
#include "cmaes.h"

using namespace libcmaes;
using namespace diffusioncma;



TEST_CASE("OptProblemTest") {

	MainConfig cfg;
	cfg.diffusion_model.n_bins = 100;
	cfg.diffusion_model.type = TypeNoModel;
	cfg.free_energy_model.n_bins = 100;
	cfg.free_energy_model.type = TypeConstantModel;
	cfg.free_energy_model.setProfile(SharedFiles::feA);

    cfg.setTransitionMatrix(SharedFiles::tmat100A);
    cfg.setLagTime(20);

    const vector<Matrix>& matrices = cfg.getTransitionMatrices();
	const vector<double>& lagtimes = cfg.getLagTimes();
	ProfileGenerator prof_gen(cfg.diffusion_model, cfg.free_energy_model);
	vector<LogLikelihood> likelihoods;
	for (size_t i = 0; i < matrices.size(); i++){
		assertion(lagtimes.at(i) > 0, "Lag time ", i+1 , " cannot be ", lagtimes.at(i), ".");
        LogLikelihood ll(matrices.at(i), lagtimes.at(i), i==0 ? cfg.verbose: false, cfg.getWeight(i));
		likelihoods.push_back(ll);
	}
    OptProblem problem(prof_gen, likelihoods, cfg.smoothing_factor);

    vector<double> x0(problem.getDimension());
    prof_gen.getFreeEnergyModel()->initializeCoefficients(x0.data(), prof_gen.getNDiffusionCoeffs(), cfg.diffusion_model);
    prof_gen.getDiffusionModel()->initializeCoefficients(x0.data() +  prof_gen.getNDiffusionCoeffs(),
			prof_gen.getNFreeEnergyCoeffs(), cfg.free_energy_model);

	CHECK_NOTHROW(problem.evaluate(x0.data(), problem.getDimension()));

} /* OptProblemTest */



TEST_CASE("OptimizationTest") {

	MainConfig cfg;
	cfg.diffusion_model.n_bins = 100;
	cfg.diffusion_model.type = TypeCosineModel;
	cfg.diffusion_model.n_coefficients = 1;
	cfg.free_energy_model.n_bins = 100;
	cfg.free_energy_model.type = TypeConstantModel;
	cfg.free_energy_model.setProfile(SharedFiles::feA);
	//cfg.optimizer = BFGS;

    cfg.setTransitionMatrix(SharedFiles::tmat100A);
    cfg.setLagTime(20);

	Solution sols = optimize(cfg);
}


FitFunc fsphere = [](const double *x, const int N)
{
  double val = 0.0;
  for (int i=0;i<N;i++)
    val += x[i]*x[i];
  return val;
};


TEST_CASE("LibcmaesTest"){
  int dim = 10; // problem dimensions.
  vector<double> x0(dim,10.0);
  double sigma = 0.1;
  //int lambda = 100; // offsprings at each generation.
  CMAParameters<> cmaparams(x0,sigma);
  //cmaparams.set_algo(BIPOP_CMAES);
  CMASolutions cmasols = cmaes<>(fsphere,cmaparams);
  /*std::cout << "best solution: " << cmasols << std::endl;
  std::cout << "optimization took " << cmasols.elapsed_time() / 1000.0 << " seconds\n";
  std::cout << cmasols.errors(cmaparams) << endl;
  std::cout << cmasols.run_status();*/

}
