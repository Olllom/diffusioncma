/*
 * LogLikelihood_test.cpp
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#include "diffusioncma/LogLikelihood.h"
#include "diffusioncma/Config.h"

#include "catch.hpp"


using namespace diffusioncma;

TEST_CASE("LogLikelihood_Construction_test") {
	Matrix test(2,2);
	REQUIRE_NOTHROW(LogLikelihood(test, 10, 1));
} /*LogLikelihood_Construction_test*/


TEST_CASE("LogLikelihood_Evaluation_test") {
	Matrix test(2,2);
	test(0,0) = 1;
	test(1,1) = 1;
	LogLikelihood A(test, 10, false);

	Profiles profiles(2);
	profiles.getLogDiff()(0,0) = 0.1;
	profiles.getLogDiff()(1,0) = 1;
	profiles.getFreeEnergy()(0,0) = 1;
	profiles.getFreeEnergy()(1,0) = 1;
	double res = A.evaluate(profiles);
	REQUIRE_FALSE(isnan(res));
} /*LogLikelihood_Construction_test*/


TEST_CASE("LogLikelihood_CompareMCDiff_test") {
	Matrix trans(3,3);
	trans <<  5,  1,  1,  0,  5,  0,  1.,  0.,  5.;

	double lag = 1;
	Matrix rate(3,3);
	rate << .1, 0.05, 0.08,0.07,0.1,0.1,0.1,0.1,0.7;
    //cout << rate;

	LogLikelihood log_like(trans, lag, true);
	double res = log_like.compute_loglike_from_rate(rate);
	REQUIRE(res == Approx(-2.117589692059722).margin(1e-5));

/* import numpy as np
t = np.array([[[5,1,1],[0,5,0],[1,0,5.]]])
n=3
rate = np.array([[.1,0.05,0.08],[0.07,0.1,0.1],[0.1,0.1,0.7]])
from mcdiff.utils import log_likelihood
log_likelihood(n, 0, t, 1, rate)
*/

} /*LogLikelihood_CompareMCDiff_test*/


TEST_CASE("LogLikelihood_BiasedRate_different"){

    Profiles p(3);
    p.getFreeEnergy() << 1.0, 2.0, 3.0;
    p.getLogDiff() << 1.0, 2.0, 1.0;

    LogLikelihood ll_unbiased(Matrix::Identity(3,3), 10, 1);
    Matrix m_unbiased = ll_unbiased.make_rate_matrix(p);

    BiasConfig bias_cfg;
    Vector biasing_pot(5);
    biasing_pot << 1.0, 2.0, 3.0, 4.0, 5.0;
    bias_cfg.setBiasingPotential(biasing_pot);
    bias_cfg.bias_type = TypeBiasingPotential;

    LogLikelihood ll_biased(Matrix::Identity(3,3), 10, 1, 1.0, bias_cfg);
    Matrix m_biased = ll_biased.make_rate_matrix(p);

    REQUIRE(not m_biased.isApprox(m_unbiased));
} /* LogLikelihood_BiasedRate_different */


TEST_CASE("LogLikelihood_Bias_ThrowWrongSizePotential"){

    Profiles p(3);
    BiasConfig bias_cfg;
    Vector biasing_pot(3);
    biasing_pot << 1.0, 2.0, 3.0;
    bias_cfg.setBiasingPotential(biasing_pot);
    bias_cfg.bias_type = TypeBiasingPotential;

    LogLikelihood ll_biased(Matrix::Identity(3,3), 10, 1, 1.0, bias_cfg);
    REQUIRE_THROWS_AS(ll_biased.make_rate_matrix(p), DiffusionCMAError);
}

TEST_CASE("LogLikelihood_CompareRate_test") {

    /*
    // this test is skipped now, because I changed the way
    // diffusion constants are converted into rate matrices.
    // previously, I had done it in the same way as mcdiff;
    // just D[i]:=Diffusion constant between bins (i) and (i+1).
    // Now I do D[i]:=Diffusion constant in bin (i) and to calculate
    // the diffusion between bins (i) and (i+1) I take the average
    // D_((i)->(i+1)) := 0.5*(D[i] + D[i+1]).


	cout << "LogLikelihood_CompareRate_test..." << endl;

	Profiles p(3);
	p.getFreeEnergy() << 1.0, 2.0, 3.0;
	p.getLogDiff() << 1.0, 2.0, 1.0;

	LogLikelihood A(Matrix::Zero(3,3), 10, 1);
	Matrix m = A.make_rate_matrix(p);

	Matrix expected(3,3);
	expected <<  -2.64872127,   4.48168907,   7.3890561,
				  1.64872127,  -8.96337814,  12.18249396,
				  1.        ,   4.48168907, -19.57155006;

	for (size_t i = 0; i < 3; i++){
		for (size_t j = 0; j < 3; j++){
			BOOST_CHECK_CLOSE(m(i,j), expected(i,j), 1e-5);
		}
	}
    */


/*
from mcdiff.utils import init_rate_matrix_pbc
n = 3
v = np.array([1,2,3])
w = np.array([1,2,1])
init_rate_matrix_pbc(n,v,w)

Out[9]:
array([[ -2.64872127,   4.48168907,   7.3890561 ],
       [  1.64872127,  -8.96337814,  12.18249396],
       [  1.        ,   4.48168907, -19.57155006]])
*/

} /*LogLikelihood_CompareRate_test*/

