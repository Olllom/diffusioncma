/*
 * Util_test.cpp
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#include "diffusioncma/Util.h"

#include "catch.hpp"

#include "SharedFiles.h"

using namespace diffusioncma;


TEST_CASE("ReadMatrix_test") {
	auto A = read_matrix(SharedFiles::tmat100A);
	REQUIRE(A.cols() == A.rows());
	REQUIRE(A.cols() == 100);
} /* ReadMatrix_test */

TEST_CASE("ReadVector_test") {
	Vector v(100);
	read_vector(v, SharedFiles::feA);
	REQUIRE(v.rows() == 100);
	REQUIRE(v.cols() == 1);
	REQUIRE(v(0,0) == Approx(0.1561482686927503638).margin(1e-5));
} /* ReadVector_test */

TEST_CASE("Assertion_test") {
	REQUIRE_THROWS_AS(assertion(false), DiffusionCMAError);
	REQUIRE_THROWS_AS(assertion(false, ""), DiffusionCMAError);
	REQUIRE_THROWS_AS(assertion(false, "Hey",2,"what"), DiffusionCMAError);
	REQUIRE_NOTHROW(assertion(true));
	REQUIRE_NOTHROW(assertion(true, "Hey",2,"what"));
} /* Assertion_test */


