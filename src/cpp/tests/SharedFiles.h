# ifndef TESTS_SHARED_FILES_H
# define TESTS_SHARED_FILES_H

#include <string>
using std::string;

namespace diffusioncma {

class SharedFiles {
public:
static const string densityA;
static const string feA;
static const string tmat100A;
};



} /* diffusioncma */

#endif /* TESTS_SHARED_FILES_H */
