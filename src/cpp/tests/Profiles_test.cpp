/*
 * Profiles_test.cpp
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */


#include "diffusioncma/Profiles.h"

#include "catch.hpp"
#include "SharedFiles.h"

using namespace diffusioncma;


TEST_CASE("Profiles_Construction_test") {
	REQUIRE_NOTHROW(Profiles(2));
} /*LogLikelihood_Construction_test*/


TEST_CASE("ProfileGenerator_Construction_test"){
	ModelConfig diffconf;
	diffconf.n_bins = 100;
	diffconf.type = TypeNoModel;
	ModelConfig feconf;
	feconf.n_bins = 100;
	feconf.type = TypeConstantModel;
	feconf.setProfile(SharedFiles::feA);
	ProfileGenerator prof_gen(diffconf, feconf);
	vector<double> coeffs(100);
	Profiles prof = prof_gen.generate(coeffs.data(), 100);
	REQUIRE(prof.getLogDiff().squaredNorm() == Approx(0.0).margin(1e-10));
	REQUIRE(prof.getFreeEnergy().squaredNorm() > 1.0);

} /*ProfileGenerator_Construction_test */

TEST_CASE("ProfileGenerator_InitProfiles_test"){
	/*cout << "ProfileGenerator_InitProfiles_test..." << endl;

	Profiles profiles(2);

	cout << "done" << endl;*/

} /*ProfileGenerator_InitProfiles_test */
