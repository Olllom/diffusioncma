/*
 * Profiles_test.cpp
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#include "diffusioncma/Model.h"

#include "catch.hpp"



#include <Eigen/Core>
#include <unsupported/Eigen/Splines>
typedef Eigen::Spline<double,1> Spline1d;
typedef Eigen::SplineFitting<Spline1d> Spline1dFitting;

using namespace diffusioncma;

TEST_CASE("Spline With Derivatives in Eigen") {
    Eigen::VectorXd x(5);
    Eigen::VectorXd y(5);
    x << 0.0, 0.25, 0.5, 0.75, 1.0;
    y << 0.0, 0.5, 1.0, 0.5, 0.0;

    Eigen::VectorXd derivatives(2);
    derivatives << 0., 0.;

    Eigen::VectorXi indices(2);
    indices << 0, x.size() - 1;

    Spline1d const& spline = Spline1dFitting::InterpolateWithDerivatives(
        y.transpose(), derivatives.transpose(), indices, 3, x);

    for (int i = 0; i < 5; ++ i){
        // CHECK(spline(x(i))(0) == y(i));
        // this is a bug in eigen that
        // should get fixed in some of the next eigen versions
    }

}

TEST_CASE("NoModel_test") {
	Vector profile(5);
	double coeffs[7] = {1,2,3,4,5,6,7};
	NoModel m;
    m.evaluate(coeffs + 2,  5, profile);
	for (size_t i = 0; i < 5; i++){
		REQUIRE(profile(i) == coeffs[i+2]);
	}
} /*NoModel_test*/


TEST_CASE("ConstantModel_test") {
    Vector profile(5);
    profile << 1,2,3,2,1;
	Vector profile2(profile);
	double coeffs[7] = {1,2,3,4,5,6,7};
    ConstantModel m(profile, 7);
    m.evaluate(coeffs + 2, 5, profile);
	for (size_t i = 0; i < 5; i++){
		REQUIRE(profile(i) == profile2(i));
	}
} /*ConstantModel_test*/


TEST_CASE("SplineModel_test") {
	Vector profile(5);
	double coeffs[7] = {1,2,3,4,5,6,7};
	SplineModel m(5);
    m.evaluate(coeffs + 2, 5, profile);
	/*for (size_t i = 0; i < 5; i++){
		CHECK(coeffs[i+2] == Approx(profile(i)).margin(0.1));
	}*/
} /*SplineModel_test*/

TEST_CASE("AsymmetricSplineModel_test") {
	Vector profile(5);
	double coeffs[7] = {1,2,3,4,5,6,7};
	AsymmetricSplineModel m(5);
    m.evaluate(coeffs + 2, 5, profile);
	/*for (size_t i = 0; i < 5; i++){
		CHECK(coeffs[i+2] == Approx(profile(i)).margin(0.1));
	}*/
} /*SplineModel_test*/

TEST_CASE("SplineModelInit_test") {
	Vector profile(9);
	profile << 1.0, 1.0, 0.5, 0.2, 1.7, 0.2, 0.5, 1.0, 1.0;
	double coeffs[7] = {0,0,0,0,0,0,0};
	ModelConfig cfg;
	cfg.type = TypeSplineModel;
    cfg.n_coefficients = 7;
	cfg.n_bins = 9;
	cfg.setProfile(profile);

    Vector spline_profile(9);
    shared_ptr<Model> m = Model::createFromConfig(cfg);
    m->initializeCoefficients(coeffs, 7, cfg);
    m->evaluate(coeffs, 7, spline_profile);
	REQUIRE((profile-spline_profile).squaredNorm() < 1e-5);
    /*cout << "p1:   " << profile << endl;
    cout << "p2:   " << spline_profile << endl;*/
} /* SplineModelInit_test */

TEST_CASE("CosModelInit_test") {
    // not testing the numbers
    Vector profile(9);
    profile << 1.0, 1.0, 0.5, 0.2, 1.7, 0.2, 0.5, 1.0, 1.0;
    double coeffs[7] = {0,0,0,0,0,0,0};
    ModelConfig cfg;
    cfg.type = TypeCosineModel;
    cfg.n_coefficients = 7;
    cfg.n_bins = 9;
    cfg.setProfile(profile);

    Vector spline_profile(9);
    shared_ptr<Model> m = Model::createFromConfig(cfg);
    m->initializeCoefficients(coeffs, 7, cfg);
    m->evaluate(coeffs, 7, spline_profile);
} /* CosModelInit_test */

TEST_CASE("RBFModelSymmetric") {
    Vector profile(5);
    double coeffs[7] = {1,2,3,4,5,6,7};
    RBFModel m(5);
    m.evaluate(coeffs + 2, 5, profile);
    for (size_t i = 0; i < profile.size(); i++){
        REQUIRE(profile[i] == profile[5-1-i]);
        REQUIRE(profile[i] > 1);
    }
}


