/*
 * integrationtest.cpp
 *
 *  Created on: 28.05.2018
 *      Author: akraemer
 */

#include "diffusioncma/Optimization.h"
#include "diffusioncma/Config.h"
#include "diffusioncma/ConfigFromArgs.h"
#include "SharedFiles.h"

using namespace diffusioncma;

int main(int argc, char* argv[]) {
	try {
		MainConfig cfg(config_from_args(argc, argv));
		if (cfg.isTransitionMatrixEmpty())
			cfg.setTransitionMatrix(SharedFiles::tmat100A);
		cfg.cma.seed = 1;
		if (cfg.getLagTimes().size() == 0)
			cfg.setLagTime(20);
		if (cfg.free_energy_model.isProfileEmpty())
			cfg.free_energy_model.setProfile(SharedFiles::feA);
		optimize(cfg);
	} catch (DiffusionCMAError& e){
		cerr << "Error in Diffusion CMA." << endl;
		cerr << e.what() << endl;
		cerr << "Exiting." << endl;
		exit(1);
	}
}

