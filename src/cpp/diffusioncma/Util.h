/*
 * Util.h
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#ifndef LIBRARY_UTIL_H_
#define LIBRARY_UTIL_H_

#include "GlobalDefinitions.h"

namespace diffusioncma {

/* Custom Error Class */
class DiffusionCMAError : public std::exception{
private:
	string m_msg;
public:
	DiffusionCMAError(string msg):
		m_msg(msg){}
	virtual const char * what () const throw(){
    	return m_msg.c_str();
    }
};


/* Custom Assertion */
inline void assertion(bool statement){
	if (not statement){
		throw DiffusionCMAError("Assertion Failed");
	}
}
inline void throw_string(stringstream& s){
	throw DiffusionCMAError(s.str());
}
template<typename T, typename ...Args> inline void throw_string(stringstream& s, const T& value, Args... args){
	s << value;
	throw_string(s, args...);
}
template<typename ...Args> inline void assertion(bool statement, Args... args){
	if (not statement){
		stringstream s;
		throw_string(s, args...);
	}
}

/*
class Assertion{
private:
	stringstream m_stream;
	bool m_assertion;
public:
	Assertion(bool assertion):
		m_assertion(assertion){
		if (assertion){
			m_stream << "Assertion Error.";
			throw DiffusionCMAError(m_stream.str());
		}
	}
	template<typename T, typename ...Args> Assertion(bool assertion, const T& value, Args... args){
		if ()
		m_stream << value;

	}
};
*/


Matrix read_matrix(const string& filename);

// read vector. if file has multiple columns, use col argument to determine which column
void read_vector(Vector& v, const string& filename, size_t col = 0);

/** @short Excerpt of vector (n elements of one half) to be printed to the console
 */
string symmetric_vector_repr(const Vector& v, size_t n = 15, bool exp=false, double scaling_factor=1.0);

} /* namespace diffusioncma */

#endif /* LIBRARY_UTIL_H_ */
