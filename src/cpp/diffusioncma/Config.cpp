/*
 * Config.cpp
 *
 *  Created on: 25.05.2018
 *      Author: akraemer
 */


#include "Config.h"

#include "GlobalDefinitions.h"
#include "Util.h"


namespace diffusioncma {

CMAConfig::CMAConfig():
		// default arguments
		popsize(-1),
		sigma0(1),
		tol(1e-3),
		seed(0){
}

ModelConfig::ModelConfig():
		type(TypeSplineModel),
		n_bins(100),
        n_coefficients(10){
}

MainConfig::MainConfig():
		profile_out("profiles.out"),
        verbose(false),
        optimizer(CMA),
        smoothing_factor(0.0)
{
}

const vector<Matrix>& MainConfig::getTransitionMatrices() const {
	assertion(m_lagtimes.size() == m_transitionMatrices.size(), "Number of lag times (",
			m_lagtimes.size(), ") does not agree with number of transition matrices (",
			m_transitionMatrices.size());
	assertion(m_transitionMatrices.size() > 0, "No transition matrices specified.");
	const size_t n = m_transitionMatrices.at(0).rows();
	assertion(n == diffusion_model.n_bins,
			"Number of bins does not match for transition matrix ("
			,n,") and diffusion profile (", diffusion_model.n_bins,").");
	assertion(n == free_energy_model.n_bins,
			"Number of bins does not match for transition matrix (",
			n, ") and free energy profile (", free_energy_model.n_bins, ").");
	return m_transitionMatrices;
}

void MainConfig::setTransitionMatrices(const vector<Matrix>& matrices){
	assertion(matrices.size()>0, "Cannot set empty list of transition matrices.");
	const size_t n = matrices.at(0).rows();
	const size_t m = matrices.at(0).cols();
	assertion(n == m, "Transition matrix 1 is not a square matrix (",
			n, "x",m,").");
	assertion((n > 0) and (m>0), "Transition matrix 1 is empty.");
	for (size_t i = 0; i < matrices.size(); i++){
		const Matrix& tmat = matrices.at(i);
		assertion((n == tmat.rows()) and (m == tmat.cols()), "Transition matrix ", i+1,
				"has dimensions (", tmat.rows(), "x", tmat.cols(),"), while first matrix has "
				, n, "x", m);
	}
	m_transitionMatrices = matrices;
	diffusion_model.n_bins = n;
	free_energy_model.n_bins = n;
}


ModelType string_to_model_type(const string& s){
	if (s == "no")
		return TypeNoModel;
	else if (s == "const")
		return TypeConstantModel;
	else if (s == "spline")
		return TypeSplineModel;
    else if (s == "cos")
        return TypeCosineModel;
    else if (s =="comp")
        return TypeCompartmentModel;
    else if (s == "rbf")
        return TypeRBFModel;
    else if (s == "a-cos")
        return TypeAsymmetricCosineSineModel;
    else if (s == "a-spline")
        return TypeAsymmetricSplineModel;
    else if (s == "a-rbf")
        return TypeAsymmetricRBFModel;
	else
		throw DiffusionCMAError("Model not recognized.");
}
string model_type_to_string(const ModelType& t){
	switch(t) {
	case TypeNoModel: return "no";
	case TypeConstantModel: return "const";
	case TypeSplineModel: return "spline";
    case TypeCosineModel: return "cos";
    case TypeCompartmentModel: return "comp";
    case TypeRBFModel: return "rbf";
    case TypeAsymmetricCosineSineModel: return "a-cos";
    case TypeAsymmetricSplineModel: return "a-spline";
    case TypeAsymmetricRBFModel: return "a-rbf";
	}
    assertion(false, "Conversion to string not defined for ModelType ", t);
}

OptimizerType string_to_optimizer_type(const string& s){
	if (s=="cma"){
		return CMA;
	}
	else if (s=="bfgs"){
		return BFGS;
	}
	else if (s=="both"){
		return BOTH;
	} else {
		throw DiffusionCMAError("Optimizer not recognized");
	}
}
string optimizer_type_to_string(const OptimizerType& t){
	switch(t) {
	case CMA: return "cma";
	case BFGS: return "bfgs";
    case BOTH: return "both";
	}
    assertion(false, "Conversion to string not defined for OptimizerType ", t);
}

BiasType string_to_bias_type(const string& s){
    if (s == "no"){
        return TypeNoBias;
    /*} else if (s == "pull"){
        return TypeConstantPullingForce;*/
    } else if (s == "potential"){
        return TypeBiasingPotential;
    } else {
        throw DiffusionCMAError("BiasType not recognized");
    }
}
string bias_type_to_string(const BiasType& b){
    switch(b){
    case TypeNoBias: return "no";
    //case TypeConstantPullingForce: return "pull";
    case TypeBiasingPotential: return "potential";
    }
    assertion(false, "Conversion to string not defined for BiasType ", b);
}

} /* namespace diffusioncma */
