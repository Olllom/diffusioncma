/*
 * Model.cpp
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#include "Model.h"

#include <math.h>
//#include <boost/math/interpolators/cubic_b_spline.hpp>
#include "Util.h"

namespace diffusioncma {

ConstantModel::ConstantModel(const Vector& constant, size_t n_coefficients):
    m_constant(symmetrize_vector(constant, n_coefficients)){
}

void SplineModel::evaluate(const double* coeffs, size_t length,
		Vector& profile) {

	assertion (length == m_nSplines, "Number of coefficients (", length,
			") does not match with number of splines (", m_nSplines, ").");

	// build spline
	double n = profile.rows();
	double mid_point = 0.5 * (n - 1.0); // center of interval [0, n-1]
	double start_point = 1; // skip outermost points
	double end_point = n - 1 - start_point;
	double step = (mid_point - start_point) / (m_nSplines-1);


	Vector x(length);
	Vector y(length);
	for (size_t i = 0; i < length; i++){
		x(i) = start_point + i * step;
        y(i) = coeffs[i];
	}
	SplineFunction spline2(x, y);

	// evaluate spline
	for (size_t i = 0; i < profile.rows(); ++i) {
		if ((i < start_point) or (i > end_point)){
			profile(i,0) = spline2(start_point);
			//cout << "cmp: " << spline(start_point) << " " << spline2(start_point) << endl;
		}
		else if (i <= mid_point){
			profile(i,0) = spline2(i);
			//cout << "cmp: " << spline(i) << " " << spline2(i) << endl;
		} else {
			profile(i,0) = spline2(n-1-i);
			//cout << "cmp: " << spline(n-1-i) << " " << spline2(n-1-i) << endl;
		}

	}


}

void CosineModel::evaluate(const double* coeffs, size_t length,
        Vector& profile) {

    assertion (length == m_nCoefficients, "Number of coefficients (", length,
            ") does not match with number of terms (", m_nCoefficients, ").");

    // build cosine function
    size_t n = profile.size();
    for (size_t j = 0; j < n; j++){
        profile(j,0) = m_hasConstantCoefficient? coeffs[0]: 0;
        for (size_t i = 1; i < length; i++){
                profile(j,0) += coeffs[m_hasConstantCoefficient? i : i-1] * cos(2*M_PI*(j+0.5)*i/float(n));
        }
    }

}

void CompartmentModel::evaluate(const double* coeffs, size_t length,
        Vector& profile) {

    assertion (length == 2*m_nCompartmentsPerHalf-1, "Number of coefficients (", length,
               ") does not match with [2*number of compartments per half - 1] (",
               m_nCompartmentsPerHalf, ").");

    // build cosine function
    size_t n = profile.size();

    // first coefficient: a constant
    for (size_t j = 0; j < n; j++){
        profile(j,0) = coeffs[0];
    }

    // each step function has 2 coefficients, one for the location of the step,
    // one for the height
    for (size_t i = 1; i < length-1; i+=2){
        // convert coefficient from R to a coefficient in the interval (0, (N-1)/2)
        double step_location = (n-1.0)*0.5* (0.5 + 0.5*tanh(coeffs[i]));
        double height = coeffs[i+1];
        //cout << step_location << " " << height << endl;
        size_t step_location_bin = round(step_location); // the bin, where the step starts
        for (size_t j = step_location_bin + 1; j < n - 1 - step_location_bin; j++){
            profile(j,0) += height;
        }
        // special handling for the bin, in which a step of the function starts,
        // to allow a continuous optimization
        double fractional_height = (0.5 + step_location - step_location_bin) * height;
        profile(step_location_bin,0) += fractional_height;
        profile(n-1-step_location_bin,0) += fractional_height;
    }
}

void RBFModel::evaluate(const double* coeffs, size_t length, Vector& profile){
    assertion (length == m_nBasisFunctions, "Number of coefficients (", length,
            ") does not match with number of terms (", m_nBasisFunctions, ").");
    size_t n = profile.size();

    double mid_point = 0.5 * (n - 1.0); // center of interval [0, n-1]
    for (size_t i = 0; i < n; i++) profile[i] = 0.0;
    for (size_t j = 0; j < m_nBasisFunctions; j++){
        // create basis point
        double basis_point = j / (m_nBasisFunctions - 1.0) * mid_point; // last point at mid point
        for (size_t i = 0; i < n/2; i++){
            profile[i] += coeffs[j]*inverse_multiquadric(fabs(i - basis_point), mid_point, m_nBasisFunctions);
        }
        for (size_t i = n/2; i < n; i++){
            profile[i] += coeffs[j]*inverse_multiquadric(fabs(n-1-i - basis_point), mid_point, m_nBasisFunctions);
        }
    }
}

void AsymmetricCosineSineModel::evaluate(const double* coeffs, size_t length, Vector& profile){
    if (m_hasConstantCoefficient){
        assertion (length == m_nCosineTerms + m_nSineTerms + 1, "Number of coefficients (", length,
                ") does not match with 1 constant term + number of cos and sin terms (", m_nCosineTerms, "+", m_nSineTerms, ").");
    } else {
        assertion (length == m_nCosineTerms + m_nSineTerms, "Number of coefficients (", length,
                ") does not match with number of cos and sin terms (", m_nCosineTerms, "+", m_nSineTerms, ").");
    }
    size_t n = profile.size();

    // build cosine function
    for (size_t j = 0; j < n; j++){
        profile(j,0) = m_hasConstantCoefficient? coeffs[0]: 0;
        for (size_t i = 1; i < ceil(length/2.0); i++){
                profile(j,0) += coeffs[m_hasConstantCoefficient? i : i-1] * cos(2*M_PI*(j+0.5)*i/float(n));
        }
        for (size_t i = ceil(length/2.0); i < length; i++){
                profile(j,0) += coeffs[m_hasConstantCoefficient? i : i-1] * sin(2*M_PI*(j+0.5)*(i- ceil(length/2.0))/float(n));
        }
    }

}

void AsymmetricSplineModel::evaluate(const double* coeffs, size_t length, Vector& profile){
    assertion (length == m_nSplines, "Number of coefficients (", length,
            ") does not match with number of terms (", m_nSplines, ").");

	// build spline
	double n = profile.rows();
	double start_point = 1; // skip outermost points
	double end_point = n - 1 - start_point;
	double step = (end_point - start_point) / (m_nSplines-1);


	Vector x(length);
	Vector y(length);
	for (size_t i = 0; i < length; i++){
		x(i) = start_point + i * step;
        y(i) = coeffs[i];
	}
	SplineFunction spline2(x, y);

	// evaluate spline
	for (size_t i = 0; i < profile.rows(); ++i) {
		if (i < start_point) {
			profile(i,0) = spline2(start_point);
			//cout << "cmp: " << spline(start_point) << " " << spline2(start_point) << endl;
		} else if ( i > end_point) {
		    profile(i,0) = spline2(end_point);
		} else {
		    profile(i,0) = spline2(i);
		}
	}
}

void AsymmetricRBFModel::evaluate(const double* coeffs, size_t length, Vector& profile){
    assertion (length == m_nBasisFunctions, "Number of coefficients (", length,
            ") does not match with number of terms (", m_nBasisFunctions, ").");
    size_t n = profile.size();

    double end_point = n - 1.0; // center of interval [0, n-1]
    for (size_t i = 0; i < n; i++) profile[i] = 0.0;
    for (size_t j = 0; j < m_nBasisFunctions; j++){
        // create basis point
        double basis_point = j / (m_nBasisFunctions - 1.0) * end_point; // last point at mid point
        for (size_t i = 0; i < n; i++){
            profile[i] += coeffs[j]*inverse_multiquadric(fabs(i - basis_point), end_point, m_nBasisFunctions);
        }
    }

}

shared_ptr<Model> Model::createFromConfig(ModelConfig& cfg, bool has_constant_coefficent){
       switch (cfg.type){
       case TypeNoModel:{
           assertion (cfg.n_bins > 0, "Model configuration has zero bins. "
        		   "Have you provided a non-empty transition matrix?");
               return make_shared<NoModel>();
       }
       case TypeConstantModel:{
               return make_shared<ConstantModel>(cfg.getProfile(), cfg.n_coefficients);
       }
       case TypeSplineModel:{
           assertion (cfg.n_coefficients > 0, "Number of splines has to be greater than zero.");
           return make_shared<SplineModel>(cfg.n_coefficients);
       }
       case TypeCosineModel:{
           assertion (cfg.n_coefficients > 0, "Number of coefficients has to be greater than zero.");
           return make_shared<CosineModel>(cfg.n_coefficients, has_constant_coefficent);
       }
       case TypeCompartmentModel:{
           assertion (cfg.n_coefficients > 0, "Number of coefficients has to be greater than zero.");
           return make_shared<CompartmentModel>(cfg.n_coefficients);
       }
       case TypeRBFModel: {
           assertion(cfg.n_coefficients > 0, "Number of coefficients has to be greater than zero.");
           return make_shared<RBFModel>(cfg.n_coefficients, has_constant_coefficent);
       }
       case TypeAsymmetricSplineModel: {
           assertion (cfg.n_coefficients > 0, "Number of coefficients has to be greater than zero.");
           return make_shared<AsymmetricSplineModel>(cfg.n_coefficients);
       }
       case TypeAsymmetricCosineSineModel: {
           assertion (cfg.n_coefficients > 0, "Number of coefficients has to be greater than zero.");
           return make_shared<AsymmetricCosineSineModel>(cfg.n_coefficients, has_constant_coefficent);
       }
       case TypeAsymmetricRBFModel: {
           assertion (cfg.n_coefficients > 0, "Number of coefficients has to be greater than zero.");
           return make_shared<AsymmetricRBFModel>(cfg.n_coefficients, has_constant_coefficent);
       }
       default: {
               throw DiffusionCMAError("No such model type.");
       }
      }
}

size_t Model::getNumCoefficientsFromConfig(ModelConfig& cfg){
    switch (cfg.type){
    case TypeNoModel:{
        assertion (cfg.n_bins > 0, "Model configuration has zero bins. "
               "Have you provided a non-empty transition matrix?");
        return cfg.n_bins;
    }
    case TypeConstantModel:{
        return 0;
    }
    case TypeCompartmentModel:{
        assertion (cfg.n_coefficients > 0, "Number of compartments has to be greater than zero.");
        return cfg.n_coefficients * 2 - 1;
        // first compartment: constant -- 1 coefficient
        // all further compartments: height and z-coordinate (interval) -- 2 coefficients
    }
    default: {
        return cfg.n_coefficients;
    }
    }
}


Vector symmetrize_vector(const Vector& v, const size_t n_coefficients){
    Vector res = v;
    // symmetrize
    res = 0.5*(v+v.reverse());
    // smoothen
    /*SplineModel tmp_model(n_coefficients);
    ModelConfig tmp_config;
    tmp_config.n_bins = res.size();
    tmp_config.n_coefficients = n_coefficients;
    tmp_config.type = TypeSplineModel;
    tmp_config.setProfile(res);
    vector<double> coeff(n_coefficients);
    initializeCoefficients(coeff.data(), n_coefficients, tmp_config);
    tmp_model.evaluate(coeff.data(), 0, n_coefficients, res);*/
    //return
    return res;
}


} /* namespace diffusioncma */
