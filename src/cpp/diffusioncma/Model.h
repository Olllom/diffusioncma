/*
 * Model.h
 *
 * A model maps coefficients to profiles. In the mcdiff code, this is done by cosine series.
 * The present code allows:
 *  - "spline" model (default) -- always symmetric wrt. the center of the data
 *  - "no" model -- no smoothing at all. Each data point is fed to the optimization.
 *     Often slow and very sensitive to noisy data.
 *  - "const" model -- No modeling. Data for a given profile is fixed during the optimization.
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#ifndef LIBRARY_MODEL_H_
#define LIBRARY_MODEL_H_

#include "GlobalDefinitions.h"
#include "Config.h"
#include "Spline.h"

namespace diffusioncma {


class Model {
public:
    // static functions
    static size_t getNumCoefficientsFromConfig(ModelConfig& cfg);
    static shared_ptr<Model> createFromConfig(ModelConfig& cfg, bool has_constant_coefficient=true);

    // member functions
	Model(){}
	virtual ~Model(){}
    virtual void evaluate(const double* coeffs, size_t length, Vector& profile) = 0;
    virtual void initializeCoefficients(double* coeffs, size_t length, ModelConfig& ) const {
        // default behavior: all zero
        for (size_t i = 0; i < length; i++){
            coeffs[i] = 0;
        }
    }
};


class NoModel: public Model {
public:
	NoModel(){}
    virtual ~NoModel() override{}
    virtual void evaluate(const double* coeffs, size_t length, Vector& profile) override {
		// copy values from coeffs to profile
        std::copy(coeffs, coeffs + length, profile.data() );
	}
    virtual void initializeCoefficients(double* coeffs, size_t length, ModelConfig& cfg) const override {
        const Vector& profile = cfg.getProfile();
        size_t n = profile.rows();
        if (n == 0)
            return;
        std::copy(profile.data(),profile.data() + length, coeffs);
    }
};

class ConstantModel: public Model{
	const Vector m_constant;
public:
    ConstantModel(const Vector& constant, size_t n_coefficients);
    virtual ~ConstantModel() override {}
    virtual void evaluate(const double* , size_t , Vector& profile) override {
		profile = m_constant;
	}
    virtual void initializeCoefficients(double* coeffs, size_t length, ModelConfig& cfg) const override {}
};

class SplineModel: public Model{
private:
	size_t m_nSplines;
public:
    SplineModel(size_t n_coefficients):
        m_nSplines(n_coefficients)
	{}
    virtual ~SplineModel() override {}
    virtual void evaluate(const double* coeffs, size_t length, Vector& profile) override;
    virtual void initializeCoefficients(double* coeffs, size_t length, ModelConfig& cfg) const override {

        const Vector& profile = cfg.getProfile();
        size_t n = profile.rows();
        if (n == 0)
            return;
        // build spline from profile (not coefficients)
        double mid_point = 0.5 * (n - 1.0); // center of interval [0, n-1]
        double start_point = 1; // skip outermost points
        double end_point = n - 1 - start_point;
        double step = (mid_point - start_point) / (length-1);

        Vector x(n);
        Vector y(n);
        for (size_t i = 0; i < n; i++){
            x(i) = i;
            y(i) = profile(i,0);
        }
        SplineFunction spline2(x, y);
        /*boost::math::cubic_b_spline<double> spline(profile.data(), n,
                0, 1, 0.0, 0.0);
*/
        // evaluate spline (at coefficient support points)
        for (size_t i = 0; i < length; i++){
            double x = start_point + i * step;
            coeffs[i] = spline2(x);
        }
    }
};


class CosineModel: public Model{
private:
    size_t m_nCoefficients;
    bool m_hasConstantCoefficient;
public:
    CosineModel(size_t n_coefficients, bool has_constant_coefficient=true):
        m_nCoefficients(n_coefficients),
        m_hasConstantCoefficient(has_constant_coefficient)
    {}
    virtual ~CosineModel() override {}
    virtual void evaluate(const double* coeffs, size_t length, Vector& profile) override;
};


class CompartmentModel: public Model{
private:
    size_t m_nCompartmentsPerHalf;
public:
    CompartmentModel(size_t n_compartments_per_half):
        m_nCompartmentsPerHalf(n_compartments_per_half)
    {}
    virtual ~CompartmentModel() override {}
    virtual void evaluate(const double* coeffs, size_t length, Vector& profile) override;
    virtual void initializeCoefficients(double* coeffs, size_t length, ModelConfig& ) const override {
        for (size_t i = 1; i < length; i+=2){
            coeffs[i] = atanh(-1.0 + 2.0*float(i)/float(length));
            coeffs[i+1] = 0.0;
        }
    }

};


/**
 * @brief The RBFModel class. Basis functions are inverse multiquadrics.
 */
class RBFModel: public Model{
private:
    size_t m_nBasisFunctions;
    bool m_hasConstantCoefficient;
public:
    RBFModel(size_t n_basis_functions, bool has_constant_coefficient=true):
            m_nBasisFunctions(n_basis_functions),
            m_hasConstantCoefficient(has_constant_coefficient)
    {}
    virtual ~RBFModel() override {};
    virtual void evaluate(const double* coeffs, size_t length, Vector& profile) override;
};


class AsymmetricCosineSineModel: public Model{
private:
    size_t m_nCosineTerms;
    size_t m_nSineTerms;
    bool m_hasConstantCoefficient;
public:
    AsymmetricCosineSineModel(size_t n_total_terms, bool has_constant_coefficient=true):
        m_nCosineTerms(n_total_terms / 2),
        m_nSineTerms(n_total_terms / 2),
        m_hasConstantCoefficient(has_constant_coefficient)
    {
        if (has_constant_coefficient){
            assertion(n_total_terms % 2 == 1, "Number of total terms in cos-sin model must be "
                                              "odd for models with constant coefficient (i.e. diffusion).");
        } else {
            assertion(n_total_terms % 2 == 0, "Number of total terms in cos-sin model must be"
                                              "even for models without constant coefficient (i.e. free energy).");
        }
    }
    virtual ~AsymmetricCosineSineModel() override {}
    virtual void evaluate(const double* coeffs, size_t length, Vector& profile) override;
};


class AsymmetricSplineModel: public Model{
private:
    size_t m_nSplines;
public:
    AsymmetricSplineModel(size_t n_splines):
            m_nSplines(n_splines){}
    virtual ~AsymmetricSplineModel() override {}
    virtual void evaluate(const double* coeffs, size_t length, Vector& profile) override;
};

/**
 * @brief The AsymmetricRBFModel class. Basis functions are inverse multiquadrics.
 */
class AsymmetricRBFModel: public Model{
private:
    size_t m_nBasisFunctions;
    bool m_hasConstantCoefficient;
public:
    AsymmetricRBFModel(size_t n_basis_functions, bool has_constant_coefficient=true):
            m_nBasisFunctions(n_basis_functions),
            m_hasConstantCoefficient(has_constant_coefficient)
    {}
    virtual ~AsymmetricRBFModel() override {};
    virtual void evaluate(const double* coeffs, size_t length, Vector& profile) override;
};

Vector symmetrize_vector(const Vector& v, const size_t n_coefficients);

/**
 * @brief Radial Basis Function
 * An inverse multiquadric phi(r) = 1/sqrt(1+(eps*r)^2).
 * The parameter eps defines the width of the peak. It is fixed to
 * eps = 2*n_basis_functions/right_boundary_point, so that between two adjacent
 * basis points the basis function has decreased to ~= 0.5
 * At the fourth-next point it has dropped to ~= 0.1;
 * this should be wide enough to prevent overfitting.
 */
inline double inverse_multiquadric(double r, double right_boundary_point, size_t n_basis_functions){
    double rbf_parameter = n_basis_functions/right_boundary_point;
    return 1.0/sqrt(1.0+pow(rbf_parameter*r, 2));
}

} /* namespace diffusioncma */

#endif /* LIBRARY_MODEL_H_ */
