#include "ConfigFromArgs.h"
#include "Config.h"

#include <boost/program_options.hpp>
namespace po = boost::program_options;

namespace diffusioncma {

MainConfig config_from_args(int argc, char *argv[]){

    MainConfig cfg;

    // set up parser
    po::options_description description("Bayesion optimization of free energy and diffusion profiles using"
            " the Covariance Matrix Adaptation - Evolution Strategy (CMA-ES). Allowed options:");
    description.add_options()("transitions,t", po::value<vector<string> >()->multitoken(),
            "MANDATORY. Files containing the transition matrices.");
    description.add_options()("lagtime,l", po::value<std::vector<double> >()->multitoken(),
            "MANDATORY. Lag times (in ps) -- number of lag times has to be the same "
            "as number of transition matrices.");
    description.add_options()("weights,w", po::value<std::vector<double> >()->multitoken(),
            "Weights for the optimization -- if specified, the number of weights has to be "
            "the same as the number of transition matrices.");
    description.add_options()("profile_out,o", po::value<string>(&cfg.profile_out)->default_value(cfg.profile_out),
            "Output file for optimized profiles.");
    description.add_options()("profile_in,i", po::value<string>(),
            "Input file for initial profiles.");
    description.add_options()("energies,e", po::value<string>(),
            "File containing the initial free energies (in units kBT).");
    description.add_options()("diffusions,d", po::value<string>(),
            "File containing the initial diffusion constants (in units bin_width^2/lagtime)");
    description.add_options()("dm",
            po::value<string>()->default_value(model_type_to_string(cfg.diffusion_model.type)),
            "Diffusion model type (no, const, spline, cos).");
    description.add_options()("em",
            po::value<string>()->default_value(model_type_to_string(cfg.free_energy_model.type)),
            "Free energy model type (no, const, spline, cos).");
    description.add_options()("ds",
            po::value<size_t>(&cfg.diffusion_model.n_coefficients)->default_value(cfg.diffusion_model.n_coefficients),
            "Diffusion model: number of coefficients.");
    description.add_options()("es",
            po::value<size_t>(&cfg.free_energy_model.n_coefficients)->default_value(cfg.free_energy_model.n_coefficients),
            "Free energy model: number of coefficients.");
    description.add_options()("smoothing",
                po::value<double>(&cfg.smoothing_factor)->default_value(cfg.smoothing_factor),
                "Factor to keep the diffusion profile smoother.");
    description.add_options()("opt",
                po::value<string>()->default_value(optimizer_type_to_string(cfg.optimizer)),
                "Optimization method (cma, bfgs, both).");
    description.add_options()("cmas",
            po::value<double>(&cfg.cma.sigma0)->default_value(cfg.cma.sigma0),
            "Initial standard deviation for CMA-ES.");
    description.add_options()("cmap",
            po::value<int>(&cfg.cma.popsize)->default_value(cfg.cma.popsize),
            "Population size for CMA-ES. A value of (-1) means that the population size gets chosen automatically.");
    description.add_options()("cmat",
            po::value<double>(&cfg.cma.tol)->default_value(cfg.cma.tol),
            "Tolerance (stopping criterion) for CMA-ES.");
    description.add_options()("cmaseed",
            po::value<uint64_t>(&cfg.cma.seed)->default_value(cfg.cma.seed),
            "Seed for CMA-ES.");
    description.add_options()("verbose,v",
            po::bool_switch(&cfg.verbose)->default_value(cfg.verbose),
            "Print optimization steps");
    description.add_options()("help,h", "produce help message");

    po::variables_map vmap;
    po::store(po::command_line_parser(argc, argv).options(description).run(), vmap);
    po::notify(vmap);

    // help message
    if (vmap.count("help")) {
        cout << description << endl;
        exit(0);
    }

    // non-trivial settings
    if (vmap.count("transitions")){
            cfg.setTransitionMatrices(vmap["transitions"].as<vector<string> >());
    }
    if (vmap.count("lagtime")){
        cfg.setLagTimes(vmap["lagtime"].as<vector<double> >());

    }
    if (vmap.count("weights")){
        cfg.setWeights(vmap["weights"].as<vector<double> >());
    }
    if (vmap.count("profile_in")){
        cfg.free_energy_model.setProfile(vmap["profile_in"].as<string>(), 1);
        cfg.diffusion_model.setProfile(vmap["profile_in"].as<string>(), 0);
    }
    if (vmap.count("energies")){
        cfg.free_energy_model.setProfile(vmap["energies"].as<string>());
    }
    if (vmap.count("diffusions")){
        cfg.diffusion_model.setProfile(vmap["diffusions"].as<string>());
    }
    if (vmap.count("opt")){
        cfg.optimizer = string_to_optimizer_type(vmap["opt"].as<string>());
    }
    if (vmap.count("dm")){
        cfg.diffusion_model.type = string_to_model_type(vmap["dm"].as<string>());
    }
    if (vmap.count("em")){
        cfg.free_energy_model.type = string_to_model_type(vmap["em"].as<string>());
    }

    return cfg;

} /* config_from_args */

} /* namespace diffusioncma */
