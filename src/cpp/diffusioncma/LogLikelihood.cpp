/*
 * LogLikelihood.cpp
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#include "LogLikelihood.h"
#include <unsupported/Eigen/MatrixFunctions>


namespace diffusioncma {

LogLikelihood::LogLikelihood(const Matrix transition_matrix, double lag_time,
                             bool verbose, double weight, const BiasConfig& bias_config):
		m_transitionMatrix(transition_matrix),
		m_lagTime(lag_time),
        m_verbose(verbose),
        m_weight(weight),
        m_biasConfig(bias_config){
    assertion(weight>=0, "LogLikelihood: Weight must be positive.");
}

LogLikelihood::~LogLikelihood() {
}

double LogLikelihood::evaluate(const Profiles& profiles){
	Matrix m = make_rate_matrix(profiles);
    double loglike = m_weight*compute_loglike_from_rate(m);
	if (m_verbose){
		profiles.print_row_string(15);
		cout << "Log-Likelihood " << std::right << std::fixed << std::setw(32)  << loglike << endl << endl;
	}
    return loglike;
}

double LogLikelihood::compute_loglike_from_rate(Matrix& m){
	//return compute_loglike_from_rate(m);
	m *= m_lagTime;
	//cout << m << endl;
	m = m.exp();				// no aliasing issues (uses a tmp)
	double threshold = 1e-10;
	for (size_t j=0; j< m.cols(); j++){	//eigen is column-major
		for (size_t i = 0; i< m.cols(); i++){
			if (m(i,j) < 1e-10){
				m(i,j) = 1e-10;
			}
		}
	}
	m = m.array().log();		// converting to an array -> element wise logarithm
    m = m_transitionMatrix.array() * m.array(); // no aliasing issues (uses a tmp)
	return m.sum();
}
Matrix LogLikelihood::make_rate_matrix(const Profiles& profiles){
	const Vector& w = profiles.getLogDiff();
    const Vector& v = profiles.getFreeEnergy();
    size_t n = profiles.getNBins();
	Matrix rate = Matrix::Zero(n,n);

    /*  Changed (07/10/18): averaging over adjacent diffusion constants
     *  (The diffusion models are defined at the centers of the bins,
     *  but the rate matrix requires the diffusion constant at the bin edges).
     *  An alternative way would have been to introduce a 0.5*bin_width-shift
     *  into the diffusion models (as in the mcdiff python package).
     */

    if (m_biasConfig.bias_type == TypeNoBias) {

        // off-diagonal
        for (size_t i = 0;  i < n-1; i++){
            rate(i+1,i) = 0.5*(exp(w(i,0))+exp(w(i+1,0))) * exp(-0.5*(v(i+1,0)-v(i,0)));
            rate(i,i+1) = 0.5*(exp(w(i,0))+exp(w(i+1,0))) * exp(-0.5*(v(i,0)-v(i+1,0)));
        }
        // corners
        rate(0,n-1) = 0.5*(exp(w(n-1,0)) + exp(w(0,0))) * exp(-0.5*(v(0,0)-v(n-1,0)));
        rate(n-1,0) = 0.5*(exp(w(n-1,0)) + exp(w(0,0))) * exp(-0.5*(v(n-1,0)-v(0,0)));

    } else if (m_biasConfig.bias_type == TypeBiasingPotential) {

        // add biasing potential to all free energy terms
        const Vector& bias = m_biasConfig.getBiasingPotential();
        assertion(bias.size() == v.size() + 2, "Biasing Potential has to have length (n_bins + 2).");

        // off-diagonal
        for (size_t i = 0;  i < n-1; i++){
            rate(i+1,i) = 0.5*(exp(w(i,0))+exp(w(i+1,0))) * exp(-0.5*(v(i+1,0) + bias(i+2,0) - v(i,0) - bias(i+1,0)));
            rate(i,i+1) = 0.5*(exp(w(i,0))+exp(w(i+1,0))) * exp(-0.5*(v(i,0) + bias(i+1,0) - v(i+1,0) - bias(i+2,0)));
        }
        // corners
        rate(0,n-1) = 0.5*(exp(w(n-1,0)) + exp(w(0,0))) * exp(-0.5*(v(0,0)+bias(1,0) - v(n-1,0)-bias(0,0)));
        rate(n-1,0) = 0.5*(exp(w(n-1,0)) + exp(w(0,0))) * exp(-0.5*(v(n-1,0)+bias(n,0)-v(0,0)-bias(n+1,0)));
    } else {
        throw DiffusionCMAError("Rate matrix convertion for biasing type not defined.");
    }

    rate(0,0) = -rate(1,0) - rate(n-1,0);
    rate(n-1,n-1) = -rate(n-2,n-1) -rate(0,n-1);
	// diagonal
	for (size_t i = 1; i < n-1; i++){
		rate(i,i) = -rate(i-1,i) - rate(i+1,i);
	}
	return rate;
}


} /* namespace diffusioncma */
