/*
 * Optimization.h
 *
 *  Created on: 24.05.2018
 *      Author: akraemer
 */

#ifndef OPTIMIZATION_H_
#define OPTIMIZATION_H_

#include <functional>
#include <limits>


#include "cmaes.h"

#include "GlobalDefinitions.h"
#include "Profiles.h"
#include "LogLikelihood.h"

using namespace std::placeholders;
using namespace libcmaes;

namespace diffusioncma {

class Logger {
private:
    string m_filename;
    std::ofstream m_file;
    bool m_enabled;
    bool m_starttime;
public:
    Logger(string filename=""):
        m_filename(filename),
        m_starttime(clock()){
        if (filename.empty()){
            m_enabled = false;
        } else{
            m_enabled = true;
            m_file.open(filename);
            m_file.precision(5);
            m_file << "#time(sec)   loglike    coefficients" << endl;
        }
    }
    ~Logger(){
        if (m_enabled){
            m_file.close();
            cout << "All coefficients saved to " << m_filename << "." << endl;
        }
    }
    void log_evaluation(const double* coeffs, int n, double loglikelihood){
        if (m_enabled){
            m_file << float(clock() - m_starttime)/CLOCKS_PER_SEC
                   << "    " << std::right << std::fixed << loglikelihood << "   ";
            for(size_t i = 0; i < n; i++){
                m_file << " " << coeffs[i] ;
            }
            m_file << endl;
        }
    }
    void log_best(const vector<double>& best_x, double best_f){
        if (m_enabled){
            m_file << "#best:   " << std::right << std::fixed << best_f << "   ";
            for (double xi:best_x)
                m_file << " " << xi;
            m_file << endl;
        }
    }
};

class OptProblem {
private:
	ProfileGenerator& m_profileGenerator;
	vector<LogLikelihood>& m_loglikelihoods;
    vector<double> m_bestX;
	double m_bestF;
    double m_smoothingFactor;
	void update_best(const double* x, double f);
    Logger m_logger;
public:
    OptProblem(ProfileGenerator& prof_gen, vector<LogLikelihood>& log_like,
               double smoothing_factor=0.0, string log_file="");
	size_t getDimension() const {
		return m_profileGenerator.getDimension();
	}
	double evaluate(const double* coeffs, const int n);
	/*
	 * interface for lbfgspp
	 */
	double operator()(const Vector& x, Vector& grad);

    Matrix computeHessian(const Vector& x);
	double getBestF() const{
		return m_bestF;
	}
	const vector<double>& getBestX() const {
		return m_bestX;
    }
    void computeErrors(const CMAConfig& cmacfg, const CMASolutions& cmasols,
                       Profiles& lower, Profiles& upper);
    Logger& getLogger() {
        return m_logger;
    }
};

class Solution {
private:
	Profiles m_profiles;
    unique_ptr<CMASolutions> m_cmasol;
    std::vector<double> m_coeff;
    unique_ptr<Profiles> m_lower;
    unique_ptr<Profiles> m_upper;
public:
    Solution(Profiles& profiles, CMASolutions& cmasol):
		m_profiles(profiles),
        m_cmasol(unique_ptr<CMASolutions>(new CMASolutions(cmasol))),
        m_coeff(cmasol.best_candidate().get_x())
    {
	}
	const Profiles& getProfiles() const {
		return m_profiles;
	}
	const std::vector<double>& getCoefficients() const {
		return m_coeff;
	}
	const CMASolutions& getCMASolutions() const {
		return *m_cmasol;
    }
    void addBounds(Profiles& lower_bound, Profiles& upper_bound){
        m_lower = unique_ptr<Profiles>(new Profiles(lower_bound));
        m_upper = unique_ptr<Profiles>(new Profiles(lower_bound));
    }
    const Profiles& getLowerBound() const {
        return *m_lower;
    }
    const Profiles& getUpperBound() const {
        return *m_upper;
    }
};

CMAParameters<> make_cma_parameters(const CMAConfig& cmaconfig, const vector<double>& x0);

Solution optimize(MainConfig& cfg);


} /* namespace diffusioncma */

#endif /* OPTIMIZATION_H_ */
