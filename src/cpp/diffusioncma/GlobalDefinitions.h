/*
 * GlobalIncludes.h
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#ifndef LIBRARY_GLOBALDEFINITIONS_H_
#define LIBRARY_GLOBALDEFINITIONS_H_

#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <memory>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <exception>
#include <math.h>
#include <functional>
#include <Eigen/Dense>

namespace diffusioncma {

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::vector;
using std::shared_ptr;
using std::make_shared;
using std::unique_ptr;
using std::stringstream;
using std::exp;
using std::bind;

using Eigen::Dynamic;
typedef Eigen::Matrix<double, Dynamic, Dynamic> Matrix;
typedef Eigen::Matrix<double, Eigen::Dynamic, 1> Vector;


} /*namespace diffusioncma */

#endif /* LIBRARY_GLOBALDEFINITIONS_H_ */
