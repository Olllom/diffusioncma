/*
 * Util.cpp
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#include "Util.h"

using std::ifstream;

namespace diffusioncma {


#define MAXBUFSIZE  (1000)

Matrix read_matrix(const string& filename)
    {
	Matrix result;
	std::fstream cin;
	cin.open(filename.c_str());
	assertion (not cin.fail(), "Failed to open file: ", filename);
	string s;
	vector <vector <double> > matrix;
	while (getline(cin, s)) {
		stringstream input(s);
		double temp;
		vector <double> currentLine;
		while (input >> temp){
			//cout << temp << " ";
			currentLine.push_back(temp);
		}
		//cout << endl;
		if (currentLine.size() > 0)
		matrix.push_back(currentLine);
	}
	size_t i = 0;
	while (matrix.at(i).size() == 0)
		i++;
	size_t n = matrix.at(i).size();
	size_t m = matrix.size() - i;
	result.resize(m, n);
	try {
		for (; i < matrix.size(); i++){
			for (size_t j = 0; j < matrix.at(i).size(); j++){
				result(i,j) = matrix.at(i).at(j);
				//cout << matrix.at(i).at(j) << " ";
			}
			//cout << endl;
		}
	} catch (std::exception& e)
	{
		throw DiffusionCMAError("List to matrix error.");
	//return false;
	}
	return result;
};


void read_vector(Vector& v, const string& filename, size_t col){
	Matrix m(read_matrix(filename));
	if (m.cols() == 1)
	//assert (m.cols() == 1);
	//assert (m.rows() == v.rows());
		v = m; // assignment is copy for Eigen matrix types
	else
		v = m.block(0,col,m.rows(),col+1);
}

string symmetric_vector_repr(const Vector& v, size_t n, bool exp, double scaling_factor)
{
	stringstream s;
	s.precision(2);
	size_t len = v.rows();
	size_t spacing = ((len/2) / n);
	for (size_t ind  = ((len/2) % n); ind < len/2; ind += spacing ){
		if (exp) {
			s << std::right << std::setw(5) <<  scaling_factor * std::exp(v(ind))  << " " ;
		} else {
			s << std::right << std::setw(5) <<  scaling_factor * v(ind)  << " " ;
		}
	}
	s << endl;
	return s.str();
}

} /* namespace diffusioncma */
