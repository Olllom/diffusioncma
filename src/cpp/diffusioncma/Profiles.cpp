/*
 * Profiles.cpp
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#include "Profiles.h"
#include <time.h>

namespace diffusioncma {

Profiles::Profiles(size_t n_bins):
	m_logDiff(n_bins),
	m_freeEnergy(n_bins),
	m_nBins(n_bins){

}

Profiles::~Profiles() {
}

string Profiles::to_string() const {
	stringstream result;
	result 	<< "#log(Diffusion/(dx^2/dt))   FreeEnergy/(kBT)   Diffusion(dx^2/dt)  NormalizedFreeEnergy/(kbT)" << endl
			<< "#========================   ================   ==================  ==========================" << endl;
	result.precision(8);
	result.setf(std::ios::fixed, std::ios::floatfield );
	Vector fe_norm = getNormalizedFreeEnergy();
	for (size_t i = 0; i < m_nBins; i++){
		result << std::right << std::setw(24) << m_logDiff(i,0) << "   ";
		result << std::right << std::setw(16) << m_freeEnergy(i,0) << "   ";
		result << std::right << std::setw(18) << exp(m_logDiff(i,0)) << "  ";
		result << std::right << std::setw(26) << fe_norm(i,0) << endl;
	}
	return result.str();
}

ProfileGenerator::ProfileGenerator(ModelConfig& diffusion_config, ModelConfig& free_energy_config):
			m_diffusionConfig(diffusion_config),
			m_freeEnergyConfig(free_energy_config),
            m_diffusionModel(Model::createFromConfig(diffusion_config, true)), // with constant coefficient
            m_freeEnergyModel(Model::createFromConfig(free_energy_config, false)), //without constant coefficient
			m_nBins(diffusion_config.n_bins),
            m_nDiffusionCoeffs(Model::getNumCoefficientsFromConfig(diffusion_config)),
            m_nFreeEnergyCoeffs(Model::getNumCoefficientsFromConfig(free_energy_config)){
	// get number of bins
	assertion (diffusion_config.n_bins == free_energy_config.n_bins,
			"Diffusion and free energy profiles have non-matching number of bins"
			" (", diffusion_config.n_bins, " and ", free_energy_config.n_bins, ", respectively).");
}

Profiles ProfileGenerator::generate(const double* coeffs, const int n) const{
    // getindex
    Profiles prof(m_nBins);
    assertion (n == m_nDiffusionCoeffs + m_nFreeEnergyCoeffs,
            "Dimension of optimization problem does not match with total number of coefficients: ",
            n, " != ", m_nDiffusionCoeffs, " + ", m_nFreeEnergyCoeffs, ".");
    // make profile from coefficients
    m_diffusionModel->evaluate(coeffs, m_nDiffusionCoeffs, prof.getLogDiff());
    m_freeEnergyModel->evaluate(coeffs + m_nDiffusionCoeffs,
             m_nFreeEnergyCoeffs, prof.getFreeEnergy());
    return prof;
}

} /* namespace diffusioncma */
