/*
 * Spline Interpolation using Eigen
 * see: https://stackoverflow.com/questions/29822041/eigen-spline-interpolation-how-to-get-spline-y-value-at-arbitray-point-x
 */

#ifndef LIBRARY_SPLINE_H_
#define LIBRARY_SPLINE_H_

#include <Eigen/Core>
#include <unsupported/Eigen/Splines>


#include "GlobalDefinitions.h"

namespace diffusioncma {

class SplineFunction {
public:
  SplineFunction(Eigen::VectorXd const &x_vec,
                 Eigen::VectorXd const &y_vec)
    : x_min(x_vec.minCoeff()),
      x_max(x_vec.maxCoeff()),
      // Spline fitting here. X values are scaled down to [0, 1] for this.
      spline_(make_spline(x_vec, y_vec))
  { }

  double operator()(double x) const {
    // x values need to be scaled down in extraction as well.
    return spline_(scaled_value(x))(0);
  }

private:
  // Helpers to scale X values down to [0, 1]
  double scaled_value(double x) const {
    return (x - x_min) / (x_max - x_min);
  }

  Vector scaled_values(Eigen::VectorXd const &x_vec) const {
    return x_vec.unaryExpr([this](double x) { return scaled_value(x); });
  }

  double x_min;
  double x_max;

  // Spline of one-dimensional "points."
  Eigen::Spline<double, 1> spline_;
  Eigen::Spline<double, 1> make_spline(const Vector& x, const Vector& y){
      auto spline = Eigen::SplineFitting<Eigen::Spline<double, 1>>::Interpolate(
                      y.transpose(),
                       // No more than cubic spline, but accept short vectors.
                      std::min<int>(x.rows() - 1, 3),
                      scaled_values(x));
/*
 *      It would be better to fix the boundary derivatives to 0.
 *      However, there is a bug in Eigen::unsupported, see
 *      https://stackoverflow.com/questions/48382939/eigen-spline-interpolation-zero-derivatives-at-ends/51311171#51311171
      Eigen::VectorXi first_and_last_index(2);
      first_and_last_index << 0, x.size()-1;
      Vector derivatives = Vector::Zero(x.size());
      cout << "make spline" << endl;
      auto spline = Eigen::SplineFitting<Eigen::Spline<double, 1> >::InterpolateWithDerivatives<Vector, Eigen::VectorXi>(
                   y.transpose(), derivatives.transpose(), first_and_last_index,
                  std::min<int>(x.rows() - 1, 3), scaled_values(x));
      cout << "done" << endl;
*/
      return spline;
  }
};


} /* namespace diffusioncma */

#endif /* LIBRARY_SPLINE_H_ */
