/*
 * Optimization.cpp
 *
 *  Created on: 24.05.2018
 *      Author: akraemer
 */

#include "Optimization.h"


#ifdef WITH_LBFGS
    #include "LBFGS.h"

    using LBFGSpp::LBFGSParam;
    using LBFGSpp::LBFGSSolver;
#endif


namespace diffusioncma {

CMAParameters<> make_cma_parameters(const CMAConfig& cmaconfig, const vector<double>& x0){
	CMAParameters<> cmaparams(x0, cmaconfig.sigma0, cmaconfig.popsize, cmaconfig.seed);
	cmaparams.set_ftolerance(cmaconfig.tol);
	return cmaparams;
}

OptProblem::OptProblem(ProfileGenerator& prof_gen, vector<LogLikelihood>& log_like,
                        double smoothing_factor, string log_file):
        m_profileGenerator(prof_gen), m_loglikelihoods(log_like),
        m_bestX(prof_gen.getDimension()),
        m_bestF(std::numeric_limits<double>::max()/2.0),
        m_smoothingFactor(smoothing_factor),
        m_logger(log_file)
{

}

void OptProblem::update_best(const double* x, double f){
	if (f < m_bestF){
		m_bestF = f;
		std::copy(x, x + getDimension(), m_bestX.data());
	}
}

double OptProblem::evaluate(const double* coeffs, const int n){
	double penalty = std::numeric_limits<double>::max()/2.0;
	const Profiles& profiles = m_profileGenerator.generate(	coeffs, n);
	double r = 0;
	for (auto &log_like: m_loglikelihoods){
        r  += (-log_like.evaluate(profiles));
		if (isnan(r) or isinf(r)) {
			return penalty;
		}
    }
    // smoothing
    const Vector& d = profiles.getLogDiff();
    for (size_t i = 0; i < d.size() - 1; i++){
        r += m_smoothingFactor*(d(i+1)-d(i))*(d(i+1)-d(i));
    }
    // update and return
	update_best(coeffs, r);
    m_logger.log_evaluation(coeffs, n, r);
	return r;
}


double OptProblem::operator()(const Vector& x, Vector& grad){
	double h = 1e-6;
	const int n = x.size();
	double f = evaluate(x.data(), n);
	for (size_t i = 0; i < n; i++){
		Vector xph = x;
		xph(i) += h;
		double fph =  evaluate(xph.data(), n);
		grad(i) = (fph - f)/h;
	}
	return f;
}

Matrix OptProblem::computeHessian(const Vector& x) {
    double h = 1e-6;
    const size_t n = x.size();
    double f = evaluate(x.data(), n);
    Matrix hessian(n,n);
    for (size_t i = 0; i < n; i++){
        Vector xph = x;
        xph(i) += h;
        double f_phei =  evaluate(xph.data(), n);
        for (size_t j = 0; j <= i; j++){
            xph(j) += h;
            double f_phei_phej = evaluate(xph.data(), n);
            xph(j) -= h;
            hessian(i,j) = (f_phei_phej - 2*f_phei + f)/(h*h);
            hessian(j,i) = hessian(i,j);
        }
    }
    return hessian;
}

void OptProblem::computeErrors(const CMAConfig& cmacfg, const CMASolutions& cmasols,
                               Profiles& lower, Profiles& upper) {

    size_t n = m_bestX.size();

    // compute 95% confidence intervals for coefficients
    CMAParameters<> cmaparams(make_cma_parameters(cmacfg, m_bestX));
    Vector errors = 2.0 * cmasols.errors(cmaparams); // 95% confidence interval = 2*sigma
    Vector optimum = Eigen::Map<Vector>(m_bestX.data(), n, 1);
    Vector lower_coeff = optimum - errors;
    Vector upper_coeff = optimum + errors;

    // generate profiles
    lower = m_profileGenerator.generate(lower_coeff.data(), n);
    upper = m_profileGenerator.generate(upper_coeff.data(), n);
}


Solution optimize(MainConfig& cfg){
	assertion (not cfg.isTransitionMatrixEmpty(), "You have to provide a non-empty transition matrix.");
    cout << "Optimizing Diffusion and Free Energy Profiles..." << endl;
    ProfileGenerator prof_gen(cfg.diffusion_model, cfg.free_energy_model);

	// make optimization problem
	const vector<Matrix>& matrices = cfg.getTransitionMatrices();
	const vector<double>& lagtimes = cfg.getLagTimes();
	vector<LogLikelihood> likelihoods;
	for (size_t i = 0; i < matrices.size(); i++){
		assertion(lagtimes.at(i) > 0, "Lag time ", i+1 , " cannot be ", lagtimes.at(i), ".");
        LogLikelihood ll(matrices.at(i), lagtimes.at(i), i==0 ? cfg.verbose: false, cfg.getWeight(i), cfg.bias);
		likelihoods.push_back(ll);
	}
    OptProblem problem(prof_gen, likelihoods, cfg.smoothing_factor, cfg.logfile);
	// make initial set of coefficients
	vector<double> x0(problem.getDimension());
    prof_gen.getFreeEnergyModel()->initializeCoefficients(x0.data(), prof_gen.getNDiffusionCoeffs(), cfg.diffusion_model);
    prof_gen.getDiffusionModel()->initializeCoefficients(x0.data() +  prof_gen.getNDiffusionCoeffs(),
			prof_gen.getNFreeEnergyCoeffs(), cfg.free_energy_model);

	CMASolutions cmasols;

	// TODO use same vector for x0 and x*, support optimizer = BOTH to run global + local optimization
	if ((cfg.optimizer == CMA) or (cfg.optimizer == BOTH)){

		cout << "STARTING CMA-ES" << endl;
		// prepare cmaes
		CMAParameters<> cmaparams(make_cma_parameters(cfg.cma, x0));
		std::function<double (const double*, const int &n)> ff = std::bind( &OptProblem::evaluate, &problem, _1, _2 );

		// do optimization
		cmasols = cmaes<>(ff, cmaparams);

		// copy optimal vector into x0, in case optimization is continued
		std::copy(problem.getBestX().data(), problem.getBestX().data() + problem.getDimension(), x0.data());

	}
	if ((cfg.optimizer == BFGS) or (cfg.optimizer == BOTH)){
#ifdef WITH_LBFGS
		// there's some stuff that I don't like about the l-bfgs implementation
		// - sometimes, the sampled parameters are all nans (this happened only
		// - the "epsilon" stopping criterion is defined as |dx| < epsilon max(1,|x|), should be absolute
		cout << "STARTING L-BFGS" << endl;
	    // Set up parameters
	    LBFGSParam<double> param;
	    param.epsilon = 1e-4;
	    param.delta = 1e-10; // to catch penalty stuff
	    param.past = 100;
	    //param.max_iterations = 100;

	    // Create solver and function object
	    LBFGSSolver<double> solver(param);

	    // Initial guess
	    Vector x = Vector::Zero(problem.getDimension());
	    std::copy(x0.data(), x0.data() + problem.getDimension(), x.data());

	    // x will be overwritten to be the best point found
	    double fx;
	    int niter = solver.minimize(problem, x, fx);

	    /*std::cout << niter << " iterations" << std::endl;
	    std::cout << "x = \n" << x.transpose() << std::endl;
	    std::cout << "f(x) = " << fx << std::endl;*/
	    //std::copy(x.data(), x.data() + problem.getDimension(), opt_coeff.data());

#else  // WITH_LBFGS
    throw DiffusionCMAError("diffusioncma was compiled without lbfgs support.");
#endif // WITH_LBFGS
	}


	// make optimal profile
    problem.getLogger().log_best(problem.getBestX(), problem.getBestF());
	Profiles opt_prof = prof_gen.generate(problem.getBestX().data(), problem.getDimension());
    cout << "Optimization finished." << endl;
	if (cfg.profile_out != ""){
        // this saves the raw coordinates in internal units
        // therefore, this part is usually not called from the python interface
        cout << "Saving final profiles to " << cfg.profile_out << "." << endl;
		opt_prof.to_file(cfg.profile_out);
    }
    Solution sol(opt_prof, cmasols);

    // add error bars
    Profiles lower_bound(opt_prof);
    Profiles upper_bound(opt_prof);
    problem.computeErrors(cfg.cma, cmasols, lower_bound, upper_bound);
    sol.addBounds(lower_bound, upper_bound);

    return sol;
}




} /* namespace diffusioncma */
