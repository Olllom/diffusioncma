#ifndef CONFIGFROMARGS_H
#define CONFIGFROMARGS_H

#include "Config.h"

namespace diffusioncma {

MainConfig config_from_args(int argc, char *argv[]);

}

#endif // CONFIGFROMARGS_H
