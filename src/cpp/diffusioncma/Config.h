/*
 * Config.h
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#ifndef LIBRARY_CONFIG_H_
#define LIBRARY_CONFIG_H_

#include "GlobalDefinitions.h"
#include "Util.h"

namespace diffusioncma {


struct CMAConfig{
public:
	CMAConfig();
	int popsize;
	double sigma0;
    double tol;
	uint64_t seed;
};

enum ModelType {
	TypeNoModel,		/* Optimize pointwise */
	TypeConstantModel,	/* No optimization */
    TypeSplineModel,		/* Symmetric Spline model */
    TypeCosineModel,        /* Symmetric Cosine Series */
    TypeCompartmentModel,    /* Piecewise constant functions */
    TypeRBFModel, /* Radial basis functions (inverse multiquadrics) */
    TypeAsymmetricSplineModel, /* Asymmetric Spline model */
    TypeAsymmetricCosineSineModel, /* Asymmetric model with the same number of cos and sin terms */
    TypeAsymmetricRBFModel /* Asymmetric model based on radial basis functions */
};

ModelType string_to_model_type(const string& s);
string model_type_to_string(const ModelType& t);

enum OptimizerType {
	CMA,
	BFGS,
	BOTH
};

OptimizerType string_to_optimizer_type(const string& s);
string optimizer_type_to_string(const OptimizerType& t);

enum BiasType {
    TypeNoBias,
    //TypeConstantPullingForce,
    TypeBiasingPotential
};

BiasType string_to_bias_type(const string& s);
string bias_type_to_string(const BiasType& b);

struct ModelConfig {
private:
	Vector m_profile;
public:
	ModelConfig();
	ModelType type;
	size_t n_bins;
    size_t n_coefficients;
	const Vector& getProfile(){
		if (m_profile.rows() != n_bins)
			m_profile = Vector::Zero(n_bins);
		return m_profile;
	}
	bool isProfileEmpty(){
		return (m_profile.cols() == 0) or (m_profile.rows() == 0);
	}
	void setProfile(const Vector& profile){
		// n_bins is set by the main configuration.
		// In this way, we can check for matching dimension upon getProfile();
		m_profile = profile;
	}
	void setProfile(const string& filename, size_t col=0){
		read_vector(m_profile, filename, col);
	}
};


struct BiasConfig {
private:
    Vector m_biasingPotential;
public:
    BiasConfig():
        //pulling_force(0),
        bias_type(TypeNoBias),
        m_biasingPotential(){}
    //double pulling_force;
    BiasType bias_type;
    /**
     * @brief Biasing potential that was applied to the permeant in the simulations
     * @return A vector of size n_bins + 2
     */
    const Vector& getBiasingPotential() const {
        return m_biasingPotential;
    }
    void setBiasingPotential(const Vector& potential){
        m_biasingPotential = potential;
    }
};

struct MainConfig {
private:
	vector<Matrix> m_transitionMatrices;
    vector<double> m_lagtimes;
    vector<double> m_weights;
public:
	MainConfig();
	ModelConfig diffusion_model;
	ModelConfig free_energy_model;
    BiasConfig bias;
	CMAConfig cma;
	string profile_out;
	bool verbose;
    string logfile;
	OptimizerType optimizer;
    double smoothing_factor;
	const vector<Matrix>& getTransitionMatrices() const;
	void setTransitionMatrices(const vector<Matrix>& matrix);
	void setTransitionMatrix(const Matrix& matrix){
		vector<Matrix> v;
		v.push_back(matrix);
		setTransitionMatrices(v);
	}
	void setTransitionMatrices(const vector<string>& filenames){
		vector<Matrix> v;
		for (auto& f: filenames)
			v.push_back(read_matrix(f));
		setTransitionMatrices(v);
	}
	void setTransitionMatrix(const string& filename){
		vector<string> v;
		v.push_back(filename);
		setTransitionMatrices(v);
	}
	bool isTransitionMatrixEmpty() const {
		if (m_transitionMatrices.size() == 0)
			return true;
		for (auto& tmat: m_transitionMatrices)
			if ((tmat.cols() == 0) or (tmat.rows() == 0))
				return true;
		return false;
	}
	const vector<double>& getLagTimes() const{
		return m_lagtimes;
	}
	void setLagTimes(const vector<double>& lagtimes){
		for (auto& lt: lagtimes){
			assertion(lt > 0, "All lag times have to be greater than zero.");
		}
		m_lagtimes = lagtimes;
	}
	void setLagTime(double lt){
		vector<double> v;
		v.push_back(lt);
		setLagTimes(v);
	}
    void setWeights(const vector<double>& weights){
        m_weights = weights;
    }
    double getWeight(size_t i) const{
        if (i < m_weights.size())
            return m_weights.at(i);
        else
            return 1.0;
    }
};

} /* namespace diffusioncma */

#endif /* LIBRARY_CONFIG_H_ */
