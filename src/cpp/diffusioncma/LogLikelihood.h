/*
 * LogLikelihood.h
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#ifndef LIBRARY_LOGLIKELIHOOD_H_
#define LIBRARY_LOGLIKELIHOOD_H_

#include "GlobalDefinitions.h"
#include "Config.h"
#include "Profiles.h"

namespace diffusioncma {

class LogLikelihood {
private:
	const Matrix m_transitionMatrix;
	double m_lagTime;
	bool m_verbose;
    double m_weight;
    BiasConfig m_biasConfig;
	//vector<Matrix> m_rateMatrices;
public:
    LogLikelihood(const Matrix transition_matrix, double lag_time,
                  bool verbose=true, double weight=1.0, const BiasConfig& bias_config=BiasConfig());
	virtual ~LogLikelihood();
	double evaluate(const Profiles& profiles);
    Matrix make_rate_matrix(const Profiles& profiles);
	double compute_loglike_from_rate(Matrix& m);
    /*Matrix compute_hessian(const Profiles& profiles) const;*/
	/*const vector<Matrix>& getRateMatrices() const {
		return m_rateMatrices;
	}*/
};

} /* namespace diffusioncma */

#endif /* LIBRARY_LOGLIKELIHOOD_H_ */
