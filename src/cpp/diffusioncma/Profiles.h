/*
 * Profiles.h
 *
 *  Created on: 23.05.2018
 *      Author: akraemer
 */

#ifndef LIBRARY_PROFILES_H_
#define LIBRARY_PROFILES_H_

#include <map>
#include <memory>

#include "GlobalDefinitions.h"
#include "Config.h"
#include "Model.h"
#include "Util.h"

using std::map;
using std::shared_ptr;
using std::make_shared;


namespace diffusioncma {

class Profiles {
private:
	Vector m_logDiff; //logarithmic diffusion = ln (D / (dx^2/dt)),
                      //where dx = bin width and dt = unit lag time
	Vector m_freeEnergy;
	size_t m_nBins;
public:
	Profiles(size_t n_bins);
	virtual ~Profiles();
	Vector& getLogDiff(){
		return m_logDiff;
	}
	Vector& getFreeEnergy(){
		return m_freeEnergy;
	}
	const Vector& getLogDiff() const{
		return m_logDiff;
	}
	const Vector& getFreeEnergy() const{
		return m_freeEnergy;
	}
    size_t getNBins() const{
        return m_nBins;
    }
	size_t size() const {
		return m_nBins;
	}
	Vector getDiffusion() const{
		// should only be used for final output
		return exp(m_logDiff.array());
	}
	Vector getNormalizedFreeEnergy() const{
		// should only be used for final output
		Vector fe = m_freeEnergy;
		Matrix::Index minRow, minCol;
		float min_fe = fe.minCoeff(&minRow, &minCol);
		fe -= Vector::Constant(fe.rows(), fe.cols(), min_fe);
		return fe;
	}
	string to_string() const;
	void to_file(string filename) const {
		std::ofstream f(filename);
		f << to_string();
		f.close();
	}
    void operator=(const Profiles& other){
        m_freeEnergy = other.getFreeEnergy();
        m_logDiff = other.getLogDiff();
        m_nBins = other.getNBins();
    }
	/** @short Human readable (output on the console)
	 */
	void print_row_string(size_t n = 15) const {
		cout.precision(2);
		cout << "D/(dx^2/dt)  " << symmetric_vector_repr(m_logDiff, n, true);
		cout << "F/(kBT)      " << symmetric_vector_repr(m_freeEnergy, n, false);
	}
};


class ProfileGenerator {
private:
	//std::vector<Profiles> m_profiles;	// multiple profiles to support multiple threads
	ModelConfig& m_diffusionConfig;
	ModelConfig& m_freeEnergyConfig;
	shared_ptr<Model> m_diffusionModel;
	shared_ptr<Model> m_freeEnergyModel;
	size_t m_nBins;
	size_t m_nDiffusionCoeffs;
    size_t m_nFreeEnergyCoeffs;
public:
    ProfileGenerator(ModelConfig& diffusion_config, ModelConfig& free_energy_config);

    Profiles generate(const double* coeffs, const int n) const;

    Profiles generate(const Vector& coeffs) const{
        return generate(coeffs.data(), coeffs.size());
    }
	/*std::vector<Profiles>& getProfiles(){
		return m_profiles;
	}*/

	size_t getDimension() const {
		return m_nDiffusionCoeffs + m_nFreeEnergyCoeffs;
	}
	size_t getNDiffusionCoeffs() const {
		return m_nDiffusionCoeffs;
	}
	size_t getNFreeEnergyCoeffs() const {
		return m_nFreeEnergyCoeffs;
	}

    const shared_ptr<Model> getDiffusionModel() const{
        return m_diffusionModel;
    }

    const shared_ptr<Model> getFreeEnergyModel() const{
        return m_freeEnergyModel;
    }
};


} /* namespace diffusioncma */

#endif /* LIBRARY_PROFILES_H_ */
