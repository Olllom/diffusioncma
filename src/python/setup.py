"""
This setup script installs the python wrapper for the c++ library.

It is invoked by cmake and should not be called from the source tree,
as a compiled binary pydiffusioncma.<some_version>.so is expected in the
same directory as this setup.py script.

The dcma package is installed by the top-level setup.py file.
"""


from setuptools import setup
import versioneer

try:
    import pydiffusioncma
except ImportError:
    print("C++ binary not compiled. Please install the code through anaconda "
          "or compile it using the top level setup.py "
          "(python setup.py compile). "
          "This setup.py is not meant to be called from the source tree. "
          "It is invoked automatically during installation of the wrappers.")

setup(
    name='pydiffusioncma',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    author='Andreas Kraemer',
    author_email='kraemer.research@gmail.com',
    description='A program to calculate diffusion and free energy profiles in membranes.',
    long_description='',
    packages=[""],
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    zip_safe=False,
    include_package_data=True,
    package_data={"": ["pydiffusioncma*.so"]},
    exclude_package_data={"": ["_version.py", "versioneer.py*"]},
    package_dir={'': '.'},
)
