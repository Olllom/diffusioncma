/*
 * Configuration.h
 *
 *  Created on: 24.05.2018
 *      Author: akraemer
 */


#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>
#include "diffusioncma/GlobalDefinitions.h"
#include "diffusioncma/Config.h"
#include "diffusioncma/Util.h"
#include "diffusioncma/Profiles.h"
#include "diffusioncma/Optimization.h"

using namespace diffusioncma;


namespace py = pybind11;

MainConfig make_config_from_kwargs(py::kwargs kwargs){

	MainConfig cfg;
	if (kwargs){
		// python-only options
		if (kwargs.contains("tmats")){
			cfg.setTransitionMatrices(py::cast<vector<Matrix> >(kwargs["tmats"]));
		}
		if (kwargs.contains("tmat")){
			cfg.setTransitionMatrix(py::cast<Matrix >(kwargs["tmat"]));
		}
		if (kwargs.contains("ener")){
			cfg.free_energy_model.setProfile(py::cast<Vector>(kwargs["ener"]));
		}
		if (kwargs.contains("diff")){
			cfg.diffusion_model.setProfile(py::cast<Vector>(kwargs["diff"]));
		}

		if (kwargs.contains("lt")){ // only one lag time
			cfg.setLagTime(py::cast<double>(kwargs["lt"]));
		}
		if (kwargs.contains("transition")){
			cfg.setTransitionMatrix(py::cast<string>(kwargs["transition"]));
		}
		//	mirrors config_from_args
		if (kwargs.contains("transitions")){
			cfg.setTransitionMatrices(py::cast<vector<string> >(kwargs["transitions"]));
		}
		if (kwargs.contains("profile_out")){
			cfg.profile_out = py::cast<string>(kwargs["profile_out"]);
		}
		if (kwargs.contains("profile_in")){
			cfg.free_energy_model.setProfile(py::cast<string>(kwargs["profile_in"]), 1);
			cfg.diffusion_model.setProfile(py::cast<string>(kwargs["profile_in"]), 1);
		}
		if (kwargs.contains("energies")){
			cfg.free_energy_model.setProfile(py::cast<string>(kwargs["energies"]), 1);
		}
		if (kwargs.contains("diffusions")){
			cfg.free_energy_model.setProfile(py::cast<string>(kwargs["diffusions"]), 1);
		}
		if (kwargs.contains("lagtime")){
			cfg.setLagTimes(py::cast<vector<double> >(kwargs["lagtime"]));
		}
        if (kwargs.contains("weights")){
            cfg.setWeights(py::cast<vector<double> >(kwargs["weights"]));
        }
		if (kwargs.contains("opt")){
			cfg.optimizer = string_to_optimizer_type(py::cast<string>(kwargs["opt"]));
		}
		if (kwargs.contains("dm")){
			cfg.diffusion_model.type = string_to_model_type(py::cast<string>(kwargs["dm"]));
		}
		if (kwargs.contains("em")){
			cfg.free_energy_model.type = string_to_model_type(py::cast<string>(kwargs["em"]));
		}
		if (kwargs.contains("ds")){
            cfg.diffusion_model.n_coefficients = py::cast<size_t>(kwargs["ds"]);
		}
		if (kwargs.contains("es")){
            cfg.free_energy_model.n_coefficients = py::cast<size_t>(kwargs["es"]);
		}
		if (kwargs.contains("cmas")){
			cfg.cma.sigma0 = py::cast<double>(kwargs["cmas"]);
		}
		if (kwargs.contains("cmap")){
			cfg.cma.popsize = py::cast<size_t>(kwargs["cmap"]);
		}
		if (kwargs.contains("cmat")){
			cfg.cma.tol = py::cast<double>(kwargs["cmat"]);
		}
		if (kwargs.contains("cmaseed")){
			cfg.cma.seed = py::cast<uint64_t>(kwargs["cmaseed"]);
		}
		if (kwargs.contains("verbose")){
			cfg.verbose = py::cast<bool>(kwargs["verbose"]);
		}
        if (kwargs.contains("bias")){
            cfg.bias.bias_type = string_to_bias_type(py::cast<string>(kwargs["bias"]));
        }
        /*if (kwargs.contains("pulling_force")){
            cfg.bias.pulling_force = py::cast<double>(kwargs["pulling_force"]);
        }*/
        if (kwargs.contains("biasing_potential")){
            cfg.bias.setBiasingPotential(py::cast<Vector>(kwargs["biasing_potential"]));
        }
        if (kwargs.contains("logfile")){
            cfg.logfile = py::cast<string>(kwargs["logfile"]);
        }
	}
	return cfg;
}


Solution optimize_wrapper(py::kwargs kwargs){
	MainConfig cfg(make_config_from_kwargs(kwargs));
	return optimize(cfg);
}

ProfileGenerator make_profile_generator(py::kwargs kwargs){
    MainConfig cfg(make_config_from_kwargs(kwargs));
    return ProfileGenerator(cfg.diffusion_model, cfg.free_energy_model);
}


PYBIND11_MODULE(pydiffusioncma, m) {
    m.doc() = R"pbdoc(
       Calculation of diffusion and free energy profiles
	   using the covariance matrix adaptation evolution strategy (CMA-ES).
    )pbdoc";

    py::class_<Profiles>(m, "Profiles")
    		.def(	py::init<size_t>(), py::arg("nbins"),
    				R"pbdoc(Profiles constructor, taking the number of bins as an input value.)pbdoc")
			.def(	"getLogDiff",
					(const Vector& (Profiles::*)() const) &Profiles::getLogDiff,
					R"pbdoc(Get the natural logarithm of the diffusion profile ,
					  where D is in units bin_width^2/lag_time)pbdoc")
    		.def(	"getRawFreeEnergy",
    				(const Vector& (Profiles::*)() const) &Profiles::getFreeEnergy,
					R"pbdoc(Get the raw free energy profile in kBT.)pbdoc")
			.def(	"getFreeEnergy",
					(Vector (Profiles::*)() const) &Profiles::getNormalizedFreeEnergy,
					R"pbdoc(Get the normalized (min = 0) free energy profile in kBT.)pbdoc")
			.def(	"getDiffusion",
					(Vector (Profiles::*)() const) &Profiles::getDiffusion,
					R"pbdoc(Get the diffusion profile in units bin_width^2/lag_time.)pbdoc");

    py::class_<Solution>(m, "Solution")
    		.def(	"getProfiles",
    				&Solution::getProfiles)
			.def(	"getCoefficients",
                    &Solution::getCoefficients)
            .def(   "getLowerBound",
                    &Solution::getLowerBound)
            .def(   "getUpperBound",
                    &Solution::getUpperBound);
    		// could use libcmaes's python bindings to also provide the CMASolution object

    py::class_<ProfileGenerator>(m, "ProfileGenerator")
            .def(   "generate",
                    (Profiles (ProfileGenerator::*)(const Vector&) const) &ProfileGenerator::generate,
                    R"pbdoc(Evaluate the cosine model for a set of raw coefficients. Note that the results has to be converted. )pbdoc")
            ;

    py::register_exception<DiffusionCMAError>(m, "DiffusionCMAError");

    m.def(	"optimize",
    		&optimize_wrapper,
			R"pbdoc(Optimize diffusion and free energy profiles.
			\nArgs:
			\nKeyword Args:
			\nReturns:
				
			)pbdoc"
    		);

    m.def( "make_profile_generator",
           &make_profile_generator,
           R"pbdoc(Make a profile generator to generate profiles from model coefficients through the Python interface.)pbdoc"
           );

#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif
}
