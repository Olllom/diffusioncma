
import pickle


def save_and_return_figure(fig, ax, outfile):
    if outfile is not None:
        fig.savefig(outfile, dpi=400, transparent=True, bbox_inches='tight')
        # pickle file to enable editing of plots
        with open(outfile + ".pickle", 'wb') as f:
            pickle.dump(ax, f)
    return ax


def plot_diffusion(some_profiles, outfile, labels=None, title=None):
    from matplotlib import pyplot as plt
    fig, ax = plt.subplots(1)
    ax.set_xlabel(r"$z$ \ [\AA]")
    ax.set_ylabel(r"Diffusion $D(z)$\ [$\times 10^{-5}$ cm$^2$/s]") # TODO: should be cm^2/s
    if title is not None:
        ax.set_title(title)

    for p, label in zip(some_profiles, labels):
        ax.plot(p.bin_centers, p.diffusion, label=label)

    ax.legend(bbox_to_anchor=(1.04, 1), loc="upper left")

    return save_and_return_figure(fig, ax, outfile)


def plot_free_energy(some_profiles, outfile, labels=None, title=None):
    from matplotlib import pyplot as plt
    fig, ax = plt.subplots(1)
    ax.set_xlabel(r"$z$ \ [\AA]")
    ax.set_ylabel(r"Free Energy $F(z)$ \ [$k_B T$]")
    if title is not None:
        ax.set_title(title)

    for p, label in zip(some_profiles, labels):
        ax.plot(p.bin_centers, p.free_energy, label=label)

    ax.legend(bbox_to_anchor=(1.04, 1), loc="upper left")

    return save_and_return_figure(fig, ax, outfile)


def plot_diffusion_statistics(average, lower, upper, outfile, percent, title=None):
    from matplotlib import pyplot as plt
    fig, ax = plt.subplots(1)
    ax.set_xlabel(r"$z$ \ [\AA]")
    ax.set_ylabel(r"Diffusion $D(z)$\ [$\times 10^{-5}$ cm/s]")
    if title is not None:
        ax.set_title(title)

    ax.fill_between(lower.bin_centers, lower.diffusion, upper.diffusion,
                    label="{}\% Percentile".format(int(percent)),
                    color="blue", alpha=0.5)
    ax.plot(average.bin_centers, average.diffusion, label="Average", color="blue")

    ax.legend(bbox_to_anchor=(1.04, 1), loc="upper left")
    return save_and_return_figure(fig, ax, outfile)


def plot_free_energy_statistics(average, lower, upper, outfile, percent, title=None):
    from matplotlib import pyplot as plt
    fig, ax = plt.subplots(1)
    ax.set_xlabel(r"$z$ \ [\AA]")
    ax.set_ylabel(r"Free Energy $F(z)$ \ [$k_B T$]")
    if title is not None:
        ax.set_title(title)

    ax.fill_between(lower.bin_centers, lower.free_energy, upper.free_energy,
                    label="{}\% Percentile".format(int(percent)),
                    color="blue", alpha=0.5)
    ax.plot(average.bin_centers, average.free_energy, label="Average", color="blue")

    ax.legend(bbox_to_anchor=(1.04, 1), loc="upper left")
    return save_and_return_figure(fig, ax, outfile)