
from ._version import get_versions
__version__ = get_versions()['version']
del get_versions


from dcma.opt import optimize, ProfileGenerator
from dcma.matrices import Transitions
from dcma.profiles import Profiles, SetOfProfiles, extrapolate_to_infinite_lag
