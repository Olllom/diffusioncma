"""
Command line interface
"""
from __future__ import print_function

import numpy as np
import click
import dcma
from dcma import profiles, optimize, tools, matrices, plotting, biasing


@click.group()
@click.version_option(version=dcma.__version__)
def main():
    """
    A program to extract diffusion profiles from molecular dynamics
    trajectories using maximum-likelihood estimation.
    """
    pass


@main.command()
@click.argument("transitions", type=click.Path(exists=True), nargs=-1)
@click.option("-o", "--outfile", type=click.Path(exists=False), prompt=True,
              help="Output file for the optimized profiles.")
@click.option("--em", default="cos",
              type=click.Choice(["cos", "spline", "comp", "const", "no",  "rbf", "a-cos", "a-rbf", "a-spline"]),
              help="Energy model (cosine series, cubic splines, compartmental "
                   "model, constant profile, "
                   "no=optimize all values independently, radial basis function,"
                   "asymmeric cos-sin series, asymmetric radial basis function, asymmetric spline)",
              show_default=True)
@click.option("--dm", default="cos",
              type=click.Choice(["cos", "spline", "comp", "const", "no", "rbf", "a-cos", "a-rbf", "a-spline"]),
              help="Diffusion model.)",
              show_default=True)
@click.option("--es", default=10, type=int,
              help="Number of terms in energy model",
              show_default=True)
@click.option("--ds", default=6, type=int,
              help="Number of terms in diffusion model",
              show_default=True)
@click.option("--plot/--no-plot", default=True,
              help="Whether to plot the optimized profiles", show_default=True)
@click.option("-v", "--verbosity", count=True,
              help="Verbosity.", show_default=True)
@click.option("-w", "--weights",  type=str,
              help="Optimization weights for the different transition matrices,"
                   " a comma-separated list of floating point numbers with no "
                   "white spaces in between.")
@click.option("--energy", type=click.Path(exists=True),
              help="File containing energy profile (for em=const).")
@click.option("--diffusion", type=click.Path(exists=True),
              help="File containing diffusion profile (for dm=const).")
@click.option("--pulled", type=float,
              help="Pulling force that was applied in the z-direction in Piconewton. "
                   "Requires --temperature option to be specified.")
@click.option("--temperature", type=float,
              help="Temperature in Kelvin.")
@click.option("--bias_file", type=click.Path(exists=True),
              help="File containing a biasing potential in units of kbT. "
                   "The file must have one column and n_bins + 2 rows.")
@click.option("--bias_function", type=str,
              help="A lambda function describing the biasing potential. The function "
                   "should map coordinates in Angstrom to a potential in kbT.")
@click.option("--logfile", type=str, default="",
              help="Print out all raw coefficients and timings in seconds.")
def opt(transitions, outfile, plot, em, dm, es, ds,
        verbosity, weights, energy, diffusion, pulled, temperature, bias_file, bias_function, logfile):
    """
    Optimize diffusion and free energy profiles. TRANSITIONS is a list
    of files containing the transition matrices, lag times, and edges.
    """
    print("Running DCMA")
    # TODO: add weights
    transition_matrices = []
    for filename in transitions:
        transition_matrices += [matrices.Transitions.from_file(filename)]
    assert len(transition_matrices) > 0, "You have to specify at least one " \
                                         "transition matrix"
    # sums directly over transition matrices, not over log likelihoods -> much more efficient
    transition_matrices = matrices.collapse_transition_matrices(transition_matrices)
    if len(transition_matrices) < len(transitions):
        print("Collapsed {} transition matrices into {} to speed "
              "up optimization.".format(len(transitions), len(transition_matrices))
              )

    if energy is not None:
        energy = np.loadtxt(energy)
    assert diffusion is None  # TODO
    assert weights is None  # TODO: a comma-separated list

    bias = biasing.Bias(pulled, bias_file, bias_function, temperature)

    profiles = optimize(
        transition_matrices, dm=dm, em=em, ds=ds, es=es, energy=energy, bias=bias, logfile=logfile

    )
    profiles.save(outfile)


@main.command()
@click.argument("finite_profiles", type=click.Path(exists=True), nargs=-1)
@click.option("-l", "--lagtimes", type=str,  default=None,
              help="A comma-separated list of floats (lag times in ps).")
@click.option("-w", "--weights", type=str,
              help="A comma-separated list of floats (weights).")
@click.option("-o", "--outfile", type=click.Path(exists=False), prompt=True,
              help="Output file for the optimized profiles.")
@click.option("--plot/--no-plot", default=True,
              help="Whether to plot the profiles.")
def x2inf(finite_profiles, lagtimes, weights, outfile, plot):
    """
    Extrapolate profiles to infinite lag times.
    FINITE_PROFILES is a list of profiles obtained using finite lag times.
    """
    assert lagtimes is not None
    lagtimes = tools.comma_string_to_list(lagtimes)
    assert len(lagtimes) == len(finite_profiles)
    profs = [profiles.Profiles.from_file(f) for f in finite_profiles]
    prof_inf = profiles.extrapolate_to_infinite_lag(profs, lagtimes, weights=None)
    prof_inf.save(outfile)


@main.command()
@click.argument("profile", type=click.Path(exists=True))
@click.option("-a", "--area_membrane", type=float, prompt=True,
              help="Cross-section area of the membrane in A^3.")
@click.option("-t", "--time_simulation", type=float, prompt=True,
              help="Length of the simulation in ns.")
@click.option("-n", "--number_permeants", type=int, prompt=True,
              help="Number of permeant molecules in the system.")
def crossings(profile, area_membrane, time_simulation, number_permeants):
    """
    Predict the expected number of crossing events for a given profile,
    given the total area of the membrane, the length of the simulation,
    and the number of permeant molecules. PROFILE is a profile file.
    """
    click.echo("THIS FEATURE IS EXPERIMENTAL. "
               "YOU ARE BETTER OFF NOT TRUSTING THOSE RESULTS.")
    prof = profiles.Profiles.from_file(profile)
    expected, min_n, max_n = prof.expected_number_of_crossings(
        number_permeants, area_membrane, time_simulation
    )
    click.echo("--------------------------------------------------")
    click.echo("The expected number of crossings is {:.1f}".format(expected))
    click.echo("The 90% confidence interval is "
               "{:.1f} - {:.1f}".format(min_n, max_n))
    click.echo("--------------------------------------------------")


@main.command()
@click.argument("profile", type=click.Path(exists=True))
@click.option("-c", "--number_crossings", type=int, prompt=True,
              help="Number of observed permeation events.")
@click.option("-t", "--time_simulation", type=float, prompt=True,
              help="Length of the simulation in ns.")
@click.option("-n", "--number_permeants", type=int, prompt=True,
              help="Number of permeant molecules in the system.")
def perm_mfpt(profile, number_crossings, time_simulation, number_permeants):
    """
    Permeability from number of crossings via the mean fist passage time.
    """
    click.echo("THIS FEATURE IS EXPERIMENTAL. "
               "YOU ARE BETTER OFF NOT TRUSTING THOSE RESULTS.")
    prof = profiles.Profiles.from_file(profile)
    perm = prof.permeability_from_mean_first_passage_time(
        number_crossings, number_permeants,
        time_simulation)

    click.echo("--------------------------------------------------")
    click.echo("The calculated permeability is {}".format(perm))
    #click.echo("The 90% confidence interval is "
    #           "{:.1f} - {:.1f}".format(min_n, max_n))
    click.echo("--------------------------------------------------")


@main.command()
@click.argument("profile_files", type=click.Path(exists=True), nargs=-1)
@click.option("-l", "--labels", multiple=True, type=str, help="Labels",
              default=None)
@click.option("-o", "--outfile_base", type=click.Path(exists=False), prompt=True,
              help="Output file basename for the plots.")
@click.option("-t", "--title", type=str, help="Plot title", default=None)
def plot(profile_files, labels, outfile_base, title):
    """
    Plot profiles. PROFILE_FILES: the filenames to plot.
    Labels for the individual profiles can be specified using the -l option
    multiple times, e.g.

        dcma plot_profile prof1.txt prof2.txt -l first -l second

    """
    if len(labels) != len(profile_files):
        click.echo("No labels given or wrong number of labels. "
                   "Using file names as labels.")
        labels = [f.replace('_','\_') for f in profile_files]

    profs = [profiles.Profiles.from_file(f) for f in profile_files]
    plotting.plot_diffusion(profs, outfile_base + "_D.png", labels,
                            title)
    plotting.plot_free_energy(profs, outfile_base + "_F.png", labels,
                              title)
    click.echo("The diffusion and free energy profiles have been saved to")
    click.echo("  " + outfile_base + "_D.png")
    click.echo("  " + outfile_base + "_F.png")
    click.echo("--------------------------------------------------------")


@main.command()
@click.argument("profile_files", type=click.Path(exists=True), nargs=-1)
@click.option("-o", "--outfile_base", type=click.Path(exists=False), prompt=True,
              help="Output file basename for the statistics.")
@click.option("-c", "--confidence", type=float, default=95,
              help="Confidence Intervals (in percent).")
def stats(profile_files, outfile_base, confidence):
    """
    Save average profiles (and confidence intervals).
    PROFILE_FILES: the filenames containing the profiles.
    """
    assert len(profile_files) > 0, "No profiles specified."

    # average
    set = profiles.SetOfProfiles.from_files(profile_files)
    average_profile = set.get_average()

    # confidence interval
    lower_percentage = (100.0 - confidence)/2.0
    upper_percentage = 100.0 - lower_percentage
    lower_profile = set.get_percentile(lower_percentage)
    upper_profile = set.get_percentile(upper_percentage)

    # save
    click.echo("--------------------------------------------------------")
    click.echo("Saving average profile and {}% confidence intervals to".format(confidence))
    click.echo("  " + outfile_base + "_av.txt")
    click.echo("  " + outfile_base + "_lower{}.txt".format(int(confidence)))
    click.echo("  " + outfile_base + "_upper{}.txt".format(int(confidence)))
    average_profile.save(outfile_base + "_av.txt")
    lower_profile.save(outfile_base + "_lower{}.txt".format(int(confidence)))
    upper_profile.save(outfile_base + "_upper{}.txt".format(int(confidence)))

    # plot
    click.echo("Plotting diffusion and free energy profiles to")
    click.echo("  " + outfile_base+"_av_ci{}_D.png".format(int(confidence)))
    click.echo("  " + outfile_base+"_av_ci{}_F.png".format(int(confidence)))
    click.echo("--------------------------------------------------------")
    plotting.plot_diffusion_statistics(
        average_profile,
        lower_profile,
        upper_profile,
        outfile_base + "_av_ci{}_D.png".format(int(confidence)),
        confidence
    )
    plotting.plot_free_energy_statistics(
        average_profile,
        lower_profile,
        upper_profile,
        outfile_base + "_av_ci{}_F.png".format(int(confidence)),
        confidence
    )
