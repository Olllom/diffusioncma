
from dcma.matrices import Transitions

import pytest

import os
import numpy as np
from dcma.tools import path_relative_to_module


def test_tmat_read():
    filename = path_relative_to_module("./tmat100.A.20.txt")
    t = Transitions.from_file(filename)
    assert t.matrix.shape == (100, 100)
    assert t.lag_time == 20
    edges = np.arange(-36.7358091, 36.7358091+1e-10, 36.7358091/50.)
    assert edges.shape == t.edges.shape
    assert np.linalg.norm(t.edges - edges) < 1e-4


def test_tmat_add():
    filename = path_relative_to_module("./tmat100.A.20.txt")
    t1 = Transitions.from_file(filename)
    t2 = Transitions.from_file(filename)
    t = t1 + t2
    assert t.matrix == pytest.approx(t1.matrix + t2.matrix)

    t_sum = sum([t1, t2])
    assert t_sum.matrix == pytest.approx(t.matrix)


def test_tmat_save_and_equals(tmpdir):
    filename = path_relative_to_module("./tmat100.A.20.txt")
    t = Transitions.from_file(filename)
    tmpfile = os.path.join(str(tmpdir), "tmat.txt")
    t.save(tmpfile)
    t_reloaded = Transitions.from_file(tmpfile)
    assert t == t_reloaded
    t_reloaded._matrix[0,0] *= 2
    assert t != t_reloaded