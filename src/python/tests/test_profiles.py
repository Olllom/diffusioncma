from dcma import profiles

import pytest
import os
import numpy as np
from dcma.tools import path_relative_to_module


def test_read_profiles(tmpdir):
    filename = path_relative_to_module("./oxygen.50.txt")
    loaded_profile = profiles.Profiles.from_file(filename)
    assert len(loaded_profile) == 100
    save_file = os.path.join(str(tmpdir), "oxygen.50.txt")
    loaded_profile.save(save_file)
    #with open(filename, 'r') as f1:
    #    with open(save_file, 'r') as f2:
    #        assert f1.read() == f2.read()
    loaded_profile2 = profiles.Profiles.from_file(save_file)
    assert loaded_profile2 == loaded_profile


def test_extrapolate_to_infinite_lag():
    lag_times = range(10,60,10)
    files = [path_relative_to_module("./oxygen.{}.txt".format(l))
             for l in lag_times]
    profiles_finite = [profiles.Profiles.from_file(f) for f in files]
    profile_infinity = profiles.extrapolate_to_infinite_lag(profiles_finite,
                                                            lag_times)
    p_inf = profile_infinity.calculate_permeability()[0]
    p_50 = profiles_finite[-1].calculate_permeability()[0]
    # difference < 5%
    assert (p_inf - p_50)/p_50 < 0.05


def test_set_of_profiles():
    filenames = [path_relative_to_module("./oxygen.{}.txt".format(l))
                 for l in [10,20,30] ]
    set = profiles.SetOfProfiles.from_files(filenames)
    assert set.diffusions.shape == (100,3)
    assert set.free_energies.shape == (100, 3)


def test_averages():
    filenames = [path_relative_to_module("./oxygen.{}.txt".format(l))
                 for l in [10,20,30] ]
    set = profiles.SetOfProfiles.from_files(filenames)
    av = set.get_average()
    assert av.diffusion.shape == (100,)
    assert av.free_energy.shape == (100,)


def test_percentile():
    filenames = [path_relative_to_module("./oxygen.{}.txt".format(l))
                 for l in [10,20,30] ]
    set = profiles.SetOfProfiles.from_files(filenames)
    perc = set.get_percentile(10)
    assert perc.diffusion.shape == (100,)
    assert perc.free_energy.shape == (100,)


def test_add():
    filenames = [path_relative_to_module("./oxygen.{}.txt".format(l))
                 for l in [10,20,30] ]
    set = profiles.SetOfProfiles.from_files(filenames)
    added = set[0] + set[1]
    assert len(added) == len(set[0])


def test_sum():
    filenames = [path_relative_to_module("./oxygen.{}.txt".format(l))
                 for l in [10,20,30] ]
    set = profiles.SetOfProfiles.from_files(filenames)
    added = sum([set[0], set[1], set[2]])
    assert len(added) == len(set[0])


def test_reference_free_energy():
    filename = path_relative_to_module("./oxygen.50.txt")
    loaded_profile = profiles.Profiles.from_file(filename)
    assert loaded_profile.reference_free_energy() == pytest.approx(loaded_profile.free_energy[0], abs=0.05)


def test_concentration_in_water_and_membrane():
    filename = path_relative_to_module("./oxygen.50.txt")
    loaded_profile = profiles.Profiles.from_file(filename)
    cw = loaded_profile.concentration_in_water(50, water_width=3, area=2500)
    cm = loaded_profile.concentration_in_membrane(50, skip=45, area=2500)
    # Test partition coefficient between water and membrane core
    assert cm/cw == pytest.approx(np.exp(-(loaded_profile.free_energy[49] - loaded_profile.free_energy[0])), abs=2)
