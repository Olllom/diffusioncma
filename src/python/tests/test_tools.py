from dcma import tools

import numpy as np


def make_converter():
    edges = np.array([-1.0, -0.5, 0.0, 0.5, 1.0])
    converter = tools.UnitConverter(edges)
    some_numbers = np.array([1.2, 0.6, 0.6, 1.2])
    return converter, some_numbers


def test_unit_converter_create():
    make_converter()


def test_unit_converter_diffusion():
    converter, d = make_converter()
    d_norm = converter.normalize_diffusion(d)
    d_denorm = converter.denormalize_diffusion(d_norm)
    assert np.all(np.absolute(d_denorm - d) < 1e-10)


def test_unit_converter_from_tmat():
    pass