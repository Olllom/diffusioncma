import dcma
import os
from dcma import opt, matrices, ProfileGenerator

import pytest

import numpy as np

from dcma.tools import path_relative_to_module


def test_optimize(tmpdir):
    print (dcma.__version__)
    print (dcma.__file__)
    print (opt.pydiffusioncma.__version__)
    print (opt.pydiffusioncma.__file__)
    logfile = os.path.join(str(tmpdir), "log.out")
    filename = path_relative_to_module("./tmat100.A.20.txt")
    tmat = matrices.Transitions.from_file(filename)
    profiles = opt.optimize(tmat, em="cos", es=4, dm="cos", ds=3, logfile=logfile)
    assert profiles.diffusion[0] == pytest.approx(6.0, abs=1.5)
    assert np.max(profiles.free_energy) == pytest.approx(12.0, abs=8.0)


def test_profile_generator():
    filename = path_relative_to_module("./tmat100.A.20.txt")
    tmat = matrices.Transitions.from_file(filename)
    generator = ProfileGenerator(tmat, em="cos", es=4, dm="cos", ds=3)
    # take the final coefficients from the optimization test
    profiles = generator(np.array([-1.81338, 2.11803, -0.0373406, -4.24391, 1.59816, -0.194229, 0.172844]))
    assert profiles.diffusion[0] == pytest.approx(6.0, abs=1.5)
    assert np.max(profiles.free_energy) == pytest.approx(12.0, abs=8.0)


def test_profile_generator_from_log():
    # try reading logs
    profgen, logs, bestx, bestf = ProfileGenerator.from_log(path_relative_to_module("log.out"))
    profgen(bestx)
    profgen(logs[0, 2:])
    assert bestx.shape == (7,)