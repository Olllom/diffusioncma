
from dcma import biasing

import pytest
import os
import numpy as np

from .test_tools import make_converter  #  edges = np.array([-1.0, -0.5, 0.0, 0.5, 1.0])


def test_bias_construct_unbiased():
    bias = biasing.Bias(pulling_force=None, bias_file=None, bias_function=None, temperature=None)
    assert bias.get_biasing_type() == "no"
    conv, _ = make_converter()
    assert bias.get_potential(conv) is None


def test_bias_construct_pull():
    bias = biasing.Bias(pulling_force=1.0, temperature=1.0)
    assert bias.get_biasing_type() == "potential"
    conv, _ = make_converter()
    assert bias.get_potential(conv) == pytest.approx(- conv.get_generalized_centers()*10/1.3806485279)


def test_bias_construct_file(tmpdir):
    filename = os.path.join(str(tmpdir), "potential.txt")
    potential = np.array([1.0, 0.1, 0.0, 0.0, 0.1, 1.0])
    np.savetxt(filename, potential.transpose())
    bias = biasing.Bias(bias_file=filename)
    assert bias.get_biasing_type() == "potential"
    conv, _ = make_converter()
    assert bias.get_potential(conv) == pytest.approx(potential)


def test_bias_construct_lambda():
    bias = biasing.Bias(bias_function="lambda z: -z")
    assert bias.get_biasing_type() == "potential"
    conv, _ = make_converter()
    assert bias.get_potential(conv) == pytest.approx([0.75, 0.75, 0.25, -0.25, -0.75, -0.75])
