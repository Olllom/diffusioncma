

# ------------- BUILD PYTHON INTERFACE -------------

find_package(pybind11 REQUIRED)

pybind11_add_module(pydiffusioncma pydiffusioncma.cpp )

# ------------- DEPENDENCIES -------------

target_link_libraries(pydiffusioncma 
    PUBLIC diffusioncma
)

# ------------- PYTHON FILES -------------

configure_file( setup.py . COPYONLY )
configure_file( ${PROJECT_SOURCE_DIR}/../versioneer.py . COPYONLY )
configure_file( setup.cfg . COPYONLY )
configure_file( dcma/_version.py . COPYONLY )

# ------------- INSTALL -------------

add_custom_target(install_python_wrappers
    COMMAND ${PYTHON_EXECUTABLE} setup.py install
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    DEPENDS pydiffusioncma
    COMMENT "Installing Python module"
)


