This example contains with transition matrices for three systems:

1) (unbiased) A water box containing 24 oxygen molecules
2) (harmonic) The same box with a harmonic restraint on the oxygen molecules
3) (pull) The same box with a constant pulling force on the oxygen molecules

Each of the systems was simulated for at least 50 ns seconds. The transition matrices
were assembled for chunks of 10 ns each.

The pulling force for 3) was 8.0 pN.

The force constant for the harmonic potential was 0.001 kcal/(mol Angstrom^2),
resulting in a 0.5 - 1 kBT potential bias.
This force constant amounts to 0.001688 = 0.001/(0.001987*298.15) (kB T)/Angstrom^2.