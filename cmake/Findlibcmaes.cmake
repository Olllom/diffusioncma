
get_filename_component(PYTHON_LIB_DIR ${PYTHON_LIBRARY} DIRECTORY)

find_path(LIBCMAES_INCLUDE_DIR 
        NAMES cmaes.h 
        PATHS
        $ENV{LIBCMAES_ROOT}/include/libcmaes
        ${LIBCMAES_ROOT}/include/libcmaes
        /usr/include
        /usr/local/include
        /opt/libcmaes/include
        ${PYTHON_INCLUDE_DIR}/libcmaes
        ${PYTHON_INCLUDE_DIR}/../libcmaes  # CONDA INCLUDE PATH
)

find_library(LIBCMAES_LIB 
	    NAMES 
        cmaes
        PATHS
        $ENV{LIBCMAES_ROOT}/lib
        ${LIBCMAES_ROOT}/lib
        ${LIBCMAES_ROOT}
        /lib/
        /lib64/
        /usr/lib
        /usr/lib64
        /usr/local/lib
        /usr/local/lib64
	    ${PYTHON_LIB_DIR}	# CONDA LIBRARY PATH
)

SET(LIBCMAES_FOUND ON)

#    Check include files
IF(NOT LIBCMAES_INCLUDE_DIR)
    SET(LIBCMAES_FOUND OFF)
    MESSAGE(STATUS "Could not find LIBCMAES include. Turning LIBCMAES_FOUND off")
ENDIF()

#    Check libraries
IF(NOT LIBCMAES_LIB)
    SET(LIBCMAES_FOUND OFF)
    MESSAGE(STATUS "Could not find LIBCMAES lib. Turning LIBCMAES_FOUND off")
ENDIF()

IF (LIBCMAES_FOUND)
  IF (NOT LIBCMAES_FIND_QUIETLY)
    MESSAGE(STATUS "Found LIBCMAES libraries: ${LIBCMAES_LIB}")
    MESSAGE(STATUS "Found LIBCMAES include: ${LIBCMAES_INCLUDE_DIR}")
  ENDIF (NOT LIBCMAES_FIND_QUIETLY)
ELSE (LIBCMAES_FOUND)
  IF (LIBCMAES_FIND_REQUIRED)
    MESSAGE(FATAL_ERROR "Could not find LIBCMAES")
  ENDIF (LIBCMAES_FIND_REQUIRED)
ENDIF (LIBCMAES_FOUND)


MARK_AS_ADVANCED(
    LIBCMAES_INCLUDE_DIR
    LIBCMAES_LIB
    LIBCMAES
)


# -------------- PROVIDE INTERFACE --------------

add_library( cmaes INTERFACE )
target_include_directories( cmaes INTERFACE ${LIBCMAES_INCLUDE_DIR} )
target_link_libraries( cmaes INTERFACE ${LIBCMAES_LIB} )
install( TARGETS cmaes EXPORT cmaes )
install ( EXPORT cmaes DESTINATION lib/cmake/cmaes )

