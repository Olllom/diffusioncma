# This cmake script applies the environment variables set by conda
#
# If conda is not used the script uses the active python binary.
# Whether or not this script is called in the process of a conda build is identified from
# the environment variable CONDA_BUILD.
#
# PYTHON is the python version in the host (not the build) environment. This difference is important, because
# python packages that are installed to the build environment get removed, while those installed to the host
# environment make it into the source tree.
#
# PREFIX is the directory of the host environment. Everything that ends up in this directory after installation
# makes it into the conda package.
#
# A full list of environment variables that get set during a conda build is found in
# https://conda.io/docs/user-guide/tasks/build-packages/environment-variables.html


macro(ASSERT test comment)
  if (NOT ${test})
    message ("Assertion failed: ${comment}")
  endif (NOT ${test})
endmacro(ASSERT)



if( DEFINED $ENV{CONDA_BUILD} )

    assert( DEFINED $ENV{PYTHON} "Environment variable PYTHON set" )
    assert( DEFINED $ENV{PREFIX} "Environment variable PREFIX set" )

    message("Conda python: " $ENV{PYTHON})
    set( PYTHON_EXECUTABLE $ENV{PYTHON} )

    message("(Conda) build prefix: " $ENV{PREFIX})
    list (APPEND CMAKE_MODULE_PATH $ENV{PREFIX}/lib/cmake)
    set( PYTHON_LIB_DIR  $ENV{PREFIX}/lib )
    set( PYTHON_INCLUDE_DIR $ENV{PREFIX}/include )

else( DEFINED $ENV{CONDA_BUILD} )

    find_package(PythonInterp REQUIRED)
    # PYTHON EXECUTABLE SET AUTOMATICALLY

    find_package(PythonLibs REQUIRED)
    # PYTHON LIBRARY SET AUTOMATICALLY

    get_filename_component(PYTHON_LIB_DIR ${PYTHON_LIBRARY} DIRECTORY)

endif( DEFINED $ENV{CONDA_BUILD} )


message("Python Executable: " ${PYTHON_EXECUTABLE})
message("Python library dir: " ${PYTHON_LIB_DIR})
message("Python include dir: " ${PYTHON_INCLUDE_DIR})
