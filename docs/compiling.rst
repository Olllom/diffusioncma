Compiling diffusioncma
----------------------

If you want to install the code (and are not planning to make any changes), you can use our anaconda binaries::

   conda install -c conda-forge diffusioncma
   dcma --help

If this works, you can stop reading here and just use diffusioncma.

If you want to make changes to the python part of the code (the `dcma` package),
install the code from conda first, then clone the github repository,
make changes, and `python setup.py install`. This will keep the C++ core in place
and apply the changes to the Python part.

If you want to make changes to the C++ part of the code, continue reading.
The core of diffusioncma is written in C++, which means that it needs to be compiled.
It is highly recommended to use a conda environment, so you do not have
to bother with compiling any of the third-party dependencies.

The recommended way of doing this is as follows

1) Install Requirements:
   Download and install anaconda from https://www.anaconda.com/download/.
   I recommend to set up a conda environment which contains all requirements, like so::

    conda create --name dcma_py36 python=3.6

   You can also set it up with python 2.7, if you prefer python 2 over python 3.

2) Activate the environment::

    source activate dcma_py36

   Note that you have to activate the environment each time you open a new terminal,
   before the installed packages can be used.
   (To deactivate a conda environment, type ``source deactivate``).

3) Add the conda-forge channel::

    conda config --add channels conda-forge

   and install the required third-party packages::

    conda install "cmake>=3.12.1" "libcmaes>=0.9.6" eigen pybind11 "pytest>=3.4" pytest-runner "click>=6.0" six scipy numpy matplotlib git


4) Fork the code on gitlab and clone to your local machine. (Alternatively, if you are not
   planning to make changes to the code, just clone from my gitlab repo)::

    git clone --recursive git@gitlab.com:Your_Gitlab_Handle/diffusioncma.git

   or::

    git clone --recursive git@gitlab.com:Olllom/diffusioncma.git

   Note the **--recursive** keyword that forces git to pull the L-BFGS code as a subpackage.

5) Enter the directory and run the setup script::

    cd diffusioncma
    python setup.py compile

   this will compile the c++ code (diffusioncma) and install the python wrapper (pydiffusioncma).
   To install the dcma package::

    python setup.py install  # this will install the dcma package


6) To test that the code was installed correctly, try the commands::

    dcma --version
    dcma --help

   If you want to be 100% sure, feel free to run the test suite::

    py.test src/python/dcma/pytest/

   which should run through in a couple of seconds.


Note
====

The setup.py supports two different commands for compiling and installing, so you don't have to recompile
the c++ core (diffusioncma) every time you make changes to the python modules (dcma).