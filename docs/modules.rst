
API
===

Profiles
-------
.. automodule:: dcma.profiles
    :noindex:
    :members:
    :undoc-members:
    :show-inheritance:


Matrices
-------
.. automodule:: dcma.matrices
    :noindex:
    :members:
    :undoc-members:
    :show-inheritance:


Optimization
-------
.. automodule:: dcma.opt
    :noindex:
    :members:
    :undoc-members:
    :show-inheritance:


Utility
-------
.. automodule:: dcma.tools
    :noindex:
    :members:
    :undoc-members:
    :show-inheritance:


Biasing
-------
.. automodule:: dcma.biasing
    :noindex:
    :members:
    :undoc-members:
    :show-inheritance:
