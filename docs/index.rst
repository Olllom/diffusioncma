Welcome to dcma's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   compiling
   modules

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
