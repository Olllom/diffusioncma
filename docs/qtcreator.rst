Using diffusioncma with the QtCreator IDE
-----------------------------------------

For working on the C++ code in diffusioncma, I recommend using QtCreator.

1) Download and install QtCreator

2) Compile the code on the command line.

   a) Install all requirements for diffusioncma (see compiling_).

      .. _compiling: ./compiling.rst

   b) Make the Release and Debug targets using cmake::

           ./make_release
           ./make_debug
      
      These scripts build a debug and a release version in the directories 'debug' and 'bin', respectively.

3) In QtCreator: File->Open File or Project open the file "src/CMakeLists.txt"

4) In the dialog, unselect all Kits and click on "Import Build from..." at the bottom.
   Select the directory "debug" and click "Import". Repeat for the "bin" directory.

5) Click "Configure Project".
   This adds a file "CMakeLists.txt.user" into the src directory, which stores all the settings.

6) Enjoy. In the lower left corner of the QtCreator GUI, you can switch between the debug and release configurations
   and between different targets. QtCreator also support code analysis tools such as ClangTidy and Clazy, and a lot
   more useful stuff.   
